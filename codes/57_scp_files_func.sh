#!/bin/bash
# *************************************
# 功能: Shell传输文件的功能优化
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-09-15
# *************************************

# 定制环境变量
login_user='root'

# 远程主机创建目录逻辑函数
remote_create_not_exist_dir_logic(){
  # 接收参数
  local remote_addr="$1"
  local dir_path="$2"

  # 创建远程主机目录
  ssh "${login_user}@${remote_addr}" "[ -d ${dir_path} ] || mkdir -p ${dir_path}"
}

# 基础传输功能判断
scp_file_with_check(){
  # 接收参数
  local h_addr="$1"
  local s_file="$2"
  local t_dir="$3"
 
  local f_name=$(echo "${s_file##*/}")
  local t_file="${t_dir}/${f_name}"

  # 判断本地文件和远程文件是否一致
  local l_md5sum=$(md5sum "${s_file}" | awk '{print $1}')
  local r_md5sum=$(ssh "${login_user}@${h_addr}" "[ -f ${t_file} ] \
                        && md5sum ${t_file}" | awk '{print $1}')
  if [ "${l_md5sum}" == "${r_md5sum}" ];then
    echo "本地文件和远程文件，内容一致，无需重复传输"
  else
    remote_create_not_exist_dir_logic "${h_addr}" "${t_dir}"
    scp "${s_file}" "${login_user}@${h_addr}:${t_file}"
  fi
}

# 获取目录所有文件
get_dir_all_file_list(){
  # 接收参数
  local dir_name="$1"

  # 
  for file in $(ls ${dir_name}); do
    if [ -d "${dir_name}/$file" ]; then
      get_dir_all_file_list "${dir_name}/${file}"
    elif [ -f "${dir_name}/$file" ]; then
     file_list="${file_list} ${dir_name}/${file}"
     # echo ${dir_name}/${file}
    fi
  done
  
  # echo "${file_list}"
}

scp_file_base_func(){
  # 接收参数
  # local file_type="$1"
  local tdir=$(echo $* | awk '{print $NF}')
  local sfile=$(echo $* | awk '{print $(NF-1)}')
  # local ip_list=$(echo $* | awk '{$NF=null;$(NF-1)=null;$1=null;print $0}')
  local ip_list=$(echo $* | awk '{$NF=null;$(NF-1)=null;print $0}')

  # 判断文件类型
  [ -d "${sfile}" ] && local file_type="dir"
  [ -f "${sfile}" ] && local file_type="file" 

  # 判断文件传输
  if [ "${file_type}" == "file" ]; then
    local scp_options=""
  elif [ "${file_type}" == "dir" ]; then
    local scp_options="-r"
  fi

  # 传输远程文件
  for ip in ${ip_list}; do
    # 注意：${scp_options} 不允许外侧有双引号(")
    # scp ${scp_options} "${sfile}" "${login_user}@${ip}:${tdir}"
    if [ "${file_type}" = "file" ]; then
      scp_file_with_check "${ip}" "${sfile}" "${tdir}"
    elif [ "${file_type}" == "dir" ]; then
      # 定制变量
      file_list=""
      get_dir_all_file_list "${sfile}"
      for file in ${file_list}; do
        local sub_path=$(dirname $(echo "${file##*${sfile}/}"))
        # [ "${sub_dir}" == "." ] && sub_dir=""
        local t_dir="${tdir}/${sub_path}"
        scp_file_with_check "${ip}" "${file}" "${t_dir}"
      done
    fi
  done
}

file_name="$1"
scp_file_base_func "10.0.0.12" "${file_name}" "/tmp" 

# get_dir_all_file_list "${file_name}"
# echo "${file_list}"
