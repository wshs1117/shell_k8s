#!/bin/bash
# *************************************
# 功能: Shell 通用文件操作
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-09-16
# *************************************

# 环境变量
host_addr="$1"
file_ops="$2"
file_type="$3"
file_name="$4"

login_user="root"

file_create_delete_logic(){
  # 接收参数
  local file_ops="$1"
  local file_type="$2"
  local file_name="$3"

  # 文件操作命令
  
  # 文件操作判断
  if [ "${file_ops}" == "create" ];then
    if [ "${file_type}" == "dir" ];then
      local ops_cmd="[ -d ${file_name} ] && rm -rf  ${file_name}/* \
                                         ||  mkdir -p ${file_name}" 
    elif [ "${file_type}" == "file" ];then
      local file_path=$(dirname ${file_name})
      local ops_cmd="[ -d ${file_path} ] || mkdir -p ${file_path};  > ${file_name}"
    fi  
  elif [ "${file_ops}" == "delete" ];then
    local ops_cmd="rm -rf  ${file_name}"
  fi
  
  echo ${ops_cmd}
}

# 文件通用操作功能函数
file_base_ops(){
  # 接收参数
  local host_addr="$1"
  local file_ops="$2"
  local file_type="$3"
  local file_name="$4"
  
  local regex_rule="^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$"
  # 执行动作
  if [ "${host_addr}" == "local" ]; then  
    local_cmd=$(file_create_delete_logic "${file_ops}" "${file_type}" "${file_name}")
    eval  ${local_cmd}
  elif [[ "${host_addr}" =~ ${regex_rule} ]]; then
    remote_cmd=$(file_create_delete_logic "${file_ops}" "${file_type}" "${file_name}")
    ssh "${login_user}@${host_addr}" "${remote_cmd}"
  fi
}

file_base_ops "${host_addr}" "${file_ops}" "${file_type}" "${file_name}"
