#!/bin/bash
# *************************************
# 功能: 集群基础环境定制
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2023-12-11
# *************************************

# 加载项目的配置属性信息
source_proj_base_conf(){
  # 获取当前项目的根路径
  root_dir=$(dirname $PWD)

  # 确保项目存在基础配置文件
  if [ ! -f $root_dir/conf/config ]; then
    cp $root_dir/tmpl/* $root_dir/conf/
  fi

  # 加载基本环境属性
  source $root_dir/conf/config
}

# 加载功能函数
source_proj_base_conf
source ${subfunc_dir}/${base_func_menu}
source ${subfunc_dir}/${base_func_usage}
source ${subfunc_dir}/${base_func_exec}
source ${subfunc_dir}/${base_func_secure}
source ${subfunc_dir}/${base_func_softs}
source ${subfunc_dir}/${base_cluster_exec}
source ${subfunc_dir}/${base_func_image}

# 自动识别操作系统类型，设定软件部署的命令
status=$(grep -i ubuntu /etc/issue)
[ -n "${status}" ] && os_type="Ubuntu" || os_type="CentOS"
cmd_type=$(get_remote_cmd_type "local" "localhost")

# 基础内容
array_target=(一键 内核 Docker Containerd CRI-O 仓库 Keepalived Haproxy Nginx 退出)

# 主函数区域
main(){
  while true
  do
    # 调用功能菜单函数
    base_cluster_menu
    read -p "请输入您要操作的选项id值: " target_id
    [ -e ${target_id} ] && target_id='100'
    local num_type=$(user_input_datatype_confirm "${target_id}")
    if [ "${num_type}" != "is_num" ]; then
      print::msg "console" "error" "输入正确格式的内容信息..."
      continue
    fi
    if [ ${#array_target[@]} -ge ${target_id} ]; then
      if [ ${array_target[$target_id-1]} == "一键" ] 
      then
        print::msg "all" "warning" "开始执行一键集群基础环境部署..."
        one_key_cluster_env
      elif [ ${array_target[$target_id-1]} == "内核" ]
      then
        print::msg "all" "warning" "开始执行K8s内核参数配置..."
        ip_list=$(user_define_create_ip_list "k8s内核参数调整")
        k8s_kernel_config "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "Docker" ]
      then
        print::msg "all" "warning" "开始执行docker软件的部署..."
        ip_list=$(user_define_create_ip_list "Docker软件部署")
        c_install_type=$(user_define_xxx_type "部署docker" "online" "offline" "${default_deploy_type}")
        docker_deploy_install "${c_install_type}" "${ip_list}"
        cri_deploy_offline "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "Containerd" ]
      then
        print::msg "all" "warning" "开始执行Containerd容器环境部署..."
        ip_list=$(user_define_create_ip_list "Containerd软件部署")
        # c_install_type=$(user_define_xxx_type "部署containerd" "online" "offline" "${default_deploy_type}")
        containerd_deploy_install "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "CRI-O" ]
      then
        print::msg "all" "warning" "开始执行CRI-O容器环境部署..."
        ip_list=$(user_define_create_ip_list "CRI-O软件部署")
        c_install_type=$(user_define_xxx_type "部署cri-o" "online" "offline" "${default_deploy_type}")
        crio_deploy_install "${c_install_type}" "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "仓库" ]
      then
        print::msg "all" "warning" "开始执行容器镜像仓库部署..."
        read -p "请输入您要确定为harbor主机的地址,只需要ip最后一位(示例: 12): " num
        [ -n "${num}" ] && num_list=$(create_ip_list "${target_net}" "${num}") \
            || num_list=$(awk '/regi/{print $1}' "${host_file}") 
        ip_addr=$(echo "${num_list}" | awk '{print $1}')
        [ -n "${ip_addr}" ] && harbor_addr="${harbor_addr+${ip_addr}}"

        read -p "请您输入harbor依赖环境部署的方式(在线-online|离线-offline):" d_type
        [ -n "${d_type}" ] && depend_type="${default_deploy_type+${d_type}}" || depend_type="${default_deploy_type}"
        read -p "请输入您要为harbor创建的用户名(示例: sswang): " u_harbor
        [ -n "${u_harbor}" ] && harbor_user="${harbor_user+${u_harbor}}"
        read -p "请输入您要为harbor创建的仓库名(示例: sswang): " p_harbor
        [ -n "${p_harbor}" ] && harbor_my_repo="${harbor_my_repo+${p_harbor}}"
        harbor_deploy_offline "${harbor_user}" "${harbor_my_repo}" "${depend_type}"
      elif [ ${array_target[$target_id-1]} == "Keepalived" ]
      then
        print::msg "all" "warning" "开始执行Keepalived高可用环境部署..."
      elif [ ${array_target[$target_id-1]} == "Haproxy" ]
      then
        print::msg "all" "warning" "开始执行Haproxy反向代理环境部署..."
      elif [ ${array_target[$target_id-1]} == "Nginx" ]
      then
        print::msg "all" "warning" "开始执行Nginx反向代理环境部署..."
      elif [ ${array_target[$target_id-1]} == "退出" ]
      then
        print::msg "all" "warning" "开始执行集群基础环境部署退出操作..."
  exit
      fi
    else
      Usage
    fi
  done
}

# 调用区域
main

