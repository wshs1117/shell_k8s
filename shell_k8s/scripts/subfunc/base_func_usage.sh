#!/bin/bash
# *************************************
# 功能: 主功能函数所依赖的信息输出函数
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2023-04-13
# *************************************
# 定制基础环境变量
log_info="正常"
log_succ="成功"
log_warn="提示"
log_erro="异常"

# 定制正常信息
log::info(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;37m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_info} | tee -a ${deploy_log_file}
}

# 定制成功信息
log::success(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;32m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_succ} | tee -a ${deploy_log_file}
}

# 定制提示信息
log::warning(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;33m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_warn} | tee -a ${deploy_log_file}
}

# 定制错误信息
log::error(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;31m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_erro} | tee -a ${deploy_log_file}
}

# 定制屏幕打印的信息格式函数
print::error(){
  # 接收参数
  local log_msg="$@"
  printf "\e[1;31m[%-10s]\e[0m: ${log_msg}\n" '  '${log_erro}
}

# 定制信息输出颜色数字值的获取函数 
get_color_num(){
  # 接收参数
  local msg_level="$1"

  if [ "${msg_level}" == "error" ]; then
    local msg_color_num="31"
    local msg_level_context="异常"
  elif [ "${msg_level}" == "warning"  ]; then
    local msg_color_num="33"
    local msg_level_context="提示"
  elif [ "${msg_level}" == "success"  ]; then
    local msg_color_num="32"
    local msg_level_context="成功"
  elif [ "${msg_level}" == "info"  ]; then
    local msg_color_num="37"
    local msg_level_context="正常"
  fi
  echo "${msg_color_num} ${msg_level_context} "
}
# 定制信息输出的功能函数
print::msg(){
  # 帮助信息：print::msg 输出类型 信息级别 输出内容
  # 信息类型：log | console | all
  # 信息级别：info | error | warning | success
  # 接收参数
  local msg_type="$1"
  local msg_level="$2"
  local log_msg=$(echo "$@" | awk '{$1=null; $2=null; print $0}'| awk '$1=$1')
  
  # 获取其他依赖信息
  read msg_color_num msg_level_context < <(get_color_num "${msg_level}")
  local msg_format="\e[1;${msg_color_num}m[  ${msg_level_context}  ]\e[0m:"
  
  # 信息输出
  if [ "${msg_type}" == "log" ];then
    printf "$(date "+%Y-%m-%d %T %Z(%z)") ${msg_format} ${log_msg}\n" >> ${deploy_log_file}
  elif [ "${msg_type}" == "console" ];then
    printf "${msg_format} ${log_msg}\n"
  elif [ "${msg_type}" == "all" ];then
    printf "$(date "+%Y-%m-%d %T %Z(%z)") ${msg_format} ${log_msg}\n" >> ${deploy_log_file}
    printf "${msg_format} ${log_msg}\n"
  fi
}

# 同步远程主机的日志文件信息
rsync_remote_log_context(){
  # 接收参数
  local remote_addr="$1"
  local sync_log_file="/tmp/deploy.log"

  # 同步日志格式
  scp ${login_user}@${remote_addr}:${sync_log_file} ${sync_log_file}.tmpl >/dev/null
  cat ${sync_log_file}.tmpl >> ${deploy_log_file}
}

# 主脚本退出功能函数
Usage(){
  # echo -e "\e[31m请输入有效的选项id\e[0m"
  # print::error "请输入有效的选项id"
  print::msg "console" "error" "请输入有效的选项id"
}

