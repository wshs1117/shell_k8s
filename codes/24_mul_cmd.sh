#!/bin/bash
# *************************************
# 功能: Shell脚本模板
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2023-12-05
# *************************************
cmd1="$1"
cmd2="$2"

echo $cmd1

echo "==="

echo "${cmd2}"
