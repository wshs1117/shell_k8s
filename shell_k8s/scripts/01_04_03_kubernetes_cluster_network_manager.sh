#!/bin/bash
# *************************************
# 功能: Kubernetes集群网络管理
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2024-04-15
# *************************************

# 加载项目的配置属性信息
source_proj_base_conf(){
  # 获取当前项目的根路径
  root_dir=$(dirname $PWD)

  # 确保项目存在基础配置文件
  if [ ! -f $root_dir/conf/config ]; then
    cp $root_dir/tmpl/* $root_dir/conf/
  fi

  # 加载基本环境属性
  source $root_dir/conf/config
}

# 加载子函数
source_proj_base_conf
source ${subfunc_dir}/${base_func_exec}
source ${subfunc_dir}/${base_func_menu}
source ${subfunc_dir}/${base_func_usage}
source ${subfunc_dir}/${base_func_image}
source ${subfunc_dir}/${k8s_subfunc_clean}
source ${subfunc_dir}/${k8s_subfunc_network}

# 基础内容
array_target=(flannel calico cilium 退出)

# 主函数
main(){
  while true
  do
    # 打印k8s集群清理管理菜单
    k8s_cluster_network_manager
    read -p "请输入您要操作的选项id值: " target_id
    [ -e ${target_id} ] && target_id='100'
    local num_type=$(user_input_datatype_confirm "${target_id}")
    if [ "${num_type}" != "is_num" ]; then
      echo -e "\e[31m请输入正确格式的内容信息...\e[0m"
      print::msg "console" "error" "请输入正确格式的内容信息..."
      continue
    fi
    if [ ${#array_target[@]} -ge ${target_id} ]; then
      if [ ${array_target[$target_id-1]} == "flannel" ]; then
         print::msg "all" "warning" "k8s集群使用Flannel网络解决方案..."
         k8s_cluster_network_deploy_user_interact "flannel"
      elif [ ${array_target[$target_id-1]} == "calico" ]; then
         print::msg "all" "warning" "k8s集群使用Calico网络解决方案..."
         k8s_cluster_network_deploy_user_interact "calico"
      elif [ ${array_target[$target_id-1]} == "cilium" ]; then
         print::msg "all" "warning" "k8s集群使用Cilium网络解决方案..."
         k8s_cluster_network_deploy_user_interact "cilium"
      elif [ ${array_target[$target_id-1]} == "退出" ]; then
         print::msg "all" "warning" "开始执行K8s集群网络管理退出操作..."
         exit
      fi
    else
      Usage
    fi
  done
}

# 执行主函数
main
