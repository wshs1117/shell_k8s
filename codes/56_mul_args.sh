#!/bin/bash
# *************************************
# 功能: Shell 函数输出多个返回值
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-09-13
# *************************************

# read key1 key2 < <(xxx)

func1(){
  local v1="value1 m"
  local v2="value2 n"
  local v3="value3 b"
  local v4="value4 d"

  echo "${v1}" "${v2}" "${v3}" "${v4}"
}

read k1 k2 k3 k4 \
        < <(func1)

echo "${k1}"
echo "${k2}"
echo "${k3}"
echo "${k4}"
