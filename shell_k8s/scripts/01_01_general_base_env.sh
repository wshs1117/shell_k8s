#!/bin/bash
# *************************************
# 功能: Shell自动化管理配置基础设施环境
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2023-04-13
# *************************************

# 加载项目的配置属性信息
source_proj_base_conf(){
  # 获取当前项目的根路径
  root_dir=$(dirname $PWD)

  # 确保项目存在基础配置文件
  if [ ! -f $root_dir/conf/config ]; then
    cp $root_dir/tmpl/* $root_dir/conf/
  fi

  # 加载基本环境属性
  source $root_dir/conf/config
}

# 基本功能
os_type=$(grep -i ubuntu /etc/issue >/dev/null && echo "Ubuntu" || echo "CentOS" )
[ "${os_type}" == "CentOS" ] && cmd_type="yum" || cmd_type="apt-get"

# 加载功能函数
source_proj_base_conf
source ${subfunc_dir}/${base_func_menu}
source ${subfunc_dir}/${base_func_usage}
source ${subfunc_dir}/${base_func_exec}

# 基础内容
array_target=(一键 基础 免密码 hosts 主机名 软件源 主机 退出)

# 主函数区域
main(){
  while true
  do
    base_env_menu
    read -p "请输入您要操作的选项id值: " target_id
    [ -e ${target_id} ] && target_id='100'
    local num_type=$(user_input_datatype_confirm "${target_id}")
    if [ "${num_type}" != "is_num" ]; then
      echo -e "\e[31m请输入正确格式的内容信息...\e[0m"
      continue
    fi
    if [ ${#array_target[@]} -ge ${target_id} ]; then
      if [ ${array_target[$target_id-1]} == "一键" ];
      then
        print::msg "all" "warning" "开始执行一键通用基本环境部署..."
        one_key_base_env
      elif [ ${array_target[$target_id-1]} == "基础" ]
      then
        print::msg "all" "warning" "开始执行基本环境部署..."
        expect_install
        sshkey_create
        hosts_create
      elif [ ${array_target[$target_id-1]} == "免密码" ]
      then
        print::msg "all" "warning" "开始执行跨主机免密码认证..."
        ip_list=$(user_define_create_ip_list "批量认证")
        # 为了保证ssh免密认证通过，主动conf/hosts和etc/hosts记录同步
        sync_conf_etc_hosts_record
        sshkey_auth_func "${ip_list}" "localhost"
      elif [ ${array_target[$target_id-1]} == "hosts" ]
      then
        print::msg "all" "warning" "开始执行同步集群hosts..."
        ip_list=$(user_define_create_ip_list "同步hosts")
        scp_hosts_file ${ip_list} "${host_file}" "${host_target_dir}"
      elif [ ${array_target[$target_id-1]} == "主机名" ]
      then
        print::msg "all" "warning" "开始执行设定集群主机名..."
        ip_list=$(user_define_create_ip_list "设定主机名")
        set_hostname ${ip_list}
      elif [ ${array_target[$target_id-1]} == "软件源" ]
      then
        print::msg "all" "warning" "开始执行更新软件源..."
        ip_list=$(user_define_create_ip_list "更新软件源")
        repo_update "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "主机" ]
      then
        print::msg "all" "warning" "开始执行一键部署指定主机通用基础环境操作..."
        ip_list=$(user_define_create_ip_list "一键部署通用基础环境")
        sshkey_auth_func "${ip_list}"
        scp_file ${ip_list} "${host_file}" "${host_target_dir}"
        set_hostname ${ip_list}
        repo_update "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "退出" ]
      then
        print::msg "console" "warning" "开始执行退出操作..."
        exit
      fi
    else
      Usage
    fi
  done
}

# 调用区域
main
