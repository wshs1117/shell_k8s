#!/bin/bash
# *************************************
# 功能: 定制CA等安全相关的文件通用函数
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-09-05
# *************************************

# 创建自建CA秘钥功能函数
create_private_key_func(){
  # 接收参数
  local key_dir="$1"
  local key_name="$2"
  local key_num="${ca_key_num}"

  # 创建秘钥文件
  openssl genrsa -out "${key_dir}/${key_name}.key" "${key_num}"
}

# 创建自建CA证书功能函数
create_private_ca_cert_func(){
  # 接收参数
  local ca_dir="$1"
  local ca_name="$2"
  local cn_name="$3"

  # 生成自建CA证书
  openssl req -x509 -new -nodes -sha512 -days "${ca_key_expire}" \
 -subj "/C=${ca_country}/ST=${ca_addr}/L=${ca_addr}/O=${ca_org}/OU=${ca_ou}/CN=${cn_name}" \
 -key "${ca_dir}/${ca_name}.key" \
 -out "${ca_dir}/${ca_name}.crt"
}

# 创建域名的签名请求文件功能函数
create_domain_req_csr_func(){
  # 接收参数
  local ca_dir="$1"
  local cn_name="$2"
  
  # 生成域名签名请求文件
  openssl req -sha512 -new \
    -subj "/C=${ca_country}/ST=${ca_addr}/L=${ca_addr}/O=${ca_org}/OU=OU=${ca_ou}/CN=${cn_name}" \
    -key "${ca_dir}/${cn_name}.key" \
    -out "${ca_dir}/${cn_name}.csr"
}

# 定制证书绑定域名信息
create_v3crt_bind_domain_func(){
  # 接收参数
  local ca_dir="$1"
  local cn_name="$2"
  
  # 定制环境变量
  local hosts_name=$(echo "${cn_name%%.*}")
  local domain_name="$(echo "${cn_name#*.}")"

  cat > "${ca_dir}/${v3_file_name}" <<-EOF
authorityKeyIdentifier=${auth_key_iden}
basicConstraints=CA:FALSE
keyUsage = ${v3_key_usage}
extendedKeyUsage = ${ext_key_usage}
subjectAltName = @alt_names

[alt_names]
DNS.1=${cn_name}
DNS.2=${domain_name}
DNS.3=${hosts_name}
EOF
}

# 定制基于请求生成证书的功能函数
create_crt_use_csr_func(){
  # 接收参数
  local ca_dir="$1"
  local ca_name="$2"
  local cn_name="$3" 
 
  # 生成证书
  openssl x509 -req -sha512 -days "${ca_key_expire}" \
    -extfile "${ca_dir}/${v3_file_name}" \
    -CA "${ca_dir}/${ca_name}.crt" \
    -CAkey "${ca_dir}/${ca_name}.key" -CAcreateserial \
    -in "${ca_dir}/${cn_name}.csr" \
    -out "${ca_dir}/${cn_name}.crt"
}

# 定制基于crt文件生成cert文件的功能函数
create_cert_use_crt_func(){
  # 接收参数
  local ca_dir="$1"
  local cn_name="$2"

  # 生成证书
  openssl x509 -inform PEM -in "${ca_dir}/${cn_name}.crt" -out "${ca_dir}/${cn_name}.cert"
}
