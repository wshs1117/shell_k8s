#!/bin/bash
# *************************************
# 功能: Shell脚本模板
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-09-03
# *************************************

# 临时环境变量
harbor_addr=$(grep register /etc/hosts | awk '{print $2}')
linshi_file="/tmp/config_test.txt"
config_file="config.toml"
harbor_k8s_repo="google_containers"

# 创建多行内容
edit_config_tls_attr(){
  # 创建tls属性配置
  cat > ${linshi_file} <<-eof
        [plugins."io.containerd.grpc.v1.cri".registry.configs."${harbor_addr}".tls]
          insecure_skip_verify = true
eof
  
  # 设置关键字  
  local keyword="registry.configs]"

  # 修改tls属性配置
  sed -i "/${keyword}/r ${linshi_file}" "${config_file}"
}

edit_config_mirrors_attr(){
  # 创建mirrors属性配置
  cat > ${linshi_file} <<-eof
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."${harbor_addr}"]
          endpoint = ["${harbor_addr}"]
eof

  # 设置关键字
  local keyword="registry.mirrors]"

  # 修改tls属性配置
  sed -i "/${keyword}/r ${linshi_file}" "${config_file}"
}

# 定制全局配置功能函数
containerd_config_edit(){
  # 修改Cgroup的管理方式
  sed -i "/SystemdCgroup/s#false#true#" "${config_file}"

  # 修改sandbox的镜像文件地址
  local images_name="${harbor_addr}/${harbor_k8s_repo}/pause:${k8s_pause_image_ver}"
  local keyword="x_image"
  sed -i "/${keyword}/s@${keyword}.*@${keyword} = \"${images_name}\"@" "${config_file}"

}

edit_config_tls_attr
edit_config_mirrors_attr
containerd_config_edit
