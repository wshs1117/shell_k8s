#!/bin/bash
# *************************************
# 功能: 以离线方式部署k8s集群的准备动作
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2023-12-20
# *************************************

# 加载项目的配置属性信息
source_proj_base_conf(){
  # 获取当前项目的根路径
  root_dir=$(dirname $PWD)

  # 确保项目存在基础配置文件
  if [ ! -f $root_dir/conf/config ]; then
    cp $root_dir/tmpl/* $root_dir/conf/
  fi

  # 加载基本环境属性
  source $root_dir/conf/config
}

# 加载功能函数
source_proj_base_conf
source ${subfunc_dir}/${base_func_menu}
source ${subfunc_dir}/${base_func_offline}
source ${subfunc_dir}/${base_func_usage}
source ${subfunc_dir}/${base_func_exec}

# 自动识别部署服务器操作系统类型，设定软件部署的命令
status=$(grep -i ubuntu /etc/issue)
[ -n "${status}" ] && os_type="Ubuntu" || os_type="CentOS"
[ "${os_type}" == "Ubuntu" ] && cmd_type="apt" || cmd_type="yum"

# 基础内容
array_target=(目录 expect docker cridockerd containerd nerdctl nginx harbor compose k8s helm ciliumcli kernel 退出)

# 主函数区域
main(){
  while true
  do
    # 调用功能菜单函数
    k8s_offline_menu
    read -p "请输入您要操作的选项id值: " target_id
    [ -e ${target_id} ] && target_id='100'
    local num_type=$(user_input_datatype_confirm "${target_id}")
    if [ "${num_type}" != "is_num" ]; then
      echo -e "\e[31m请输入正确格式的内容信息...\e[0m"
      print::msg "console" "error" "请输入正确格式的内容信息..."
      continue
    fi
    if [ ${#array_target[@]} -ge ${target_id} ]; then
      if [ ${array_target[$target_id-1]} == "目录" ]; then
        print::msg "all" "warning" "开始执行一键准备目录结构环境操作..."
        offline_dir_create
      elif [ ${array_target[$target_id-1]} == "expect" ]; then
        print::msg "all" "warning" "开始执行获取expect操作..."
        get_offline_expect
      elif [ ${array_target[$target_id-1]} == "docker" ]; then
        print::msg "all" "warning" "开始执行获取docker操作..."
        get_user_define_softs_version_logic "docker" "${docker_ver}"
        docker_arg="${soft_version}"
        get_offline_docker "${docker_arg}"
      elif [ ${array_target[$target_id-1]} == "cridockerd" ]; then
        print::msg "all" "warning" "开始执行获取cri-dockerd操作..."
        get_user_define_softs_version_logic "cridockerd" "${cri_dockerd_ver}"
        cri_dockerd_arg="${soft_version}"
        get_offline_cridockerd "${cri_dockerd_arg}"
      elif [ ${array_target[$target_id-1]} == "containerd" ]; then
        print::msg "all" "warning" "开始执行获取containerd操作..."
        get_user_define_softs_version_logic "containerd" "${containerd_ver}"
        containerd_arg="${soft_version}"
        get_offline_containerd "${containerd_arg}"
      elif [ ${array_target[$target_id-1]} == "nerdctl" ]; then
        print::msg "all" "warning" "开始执行获取nerdctl操作..."
        get_user_define_softs_version_logic "nerdctl" "${nerdctl_ver}"
        nerdctl_arg="${soft_version}"
        get_offline_nerdctl "${nerdctl_arg}"
      elif [ ${array_target[$target_id-1]} == "nginx" ]; then
        print::msg "all" "warning" "开始执行获取nginx操作..."
        get_user_define_softs_version_logic "nginx" "${nginx_ver}"
        nginx_arg="${soft_version}"
        get_offline_nginx "${nginx_arg}"
      elif [ ${array_target[$target_id-1]} == "harbor" ]; then
        print::msg "all" "warning" "开始执行获取harbor操作..."
        get_user_define_softs_version_logic "harbor" "${harbor_version}"
        harbor_arg="${soft_version}"
        get_offline_harbor "${harbor_arg}"
      elif [ ${array_target[$target_id-1]} == "compose" ]; then
        print::msg "all" "warning" "开始执行获取docker-compose操作..."
        get_user_define_softs_version_logic "compose" "${compose_version}"
        compose_arg="${soft_version}"
        get_offline_compose "${compose_arg}"
      elif [ ${array_target[$target_id-1]} == "k8s" ]; then
        print::msg "all" "warning" "开始执行获取kubernetes操作..."
        get_status=$(user_define_xxx_type "是否需要指定获取离线的kubernetes版本" "yes" "no" "no")
        get_offline_k8s "${get_status}"
      elif [ ${array_target[$target_id-1]} == "helm" ]; then
        print::msg "all" "warning" "开始执行获取helm操作..."
        get_user_define_softs_version_logic "helm" "${helm_version}"
        helm_arg="${soft_version}"
        get_offline_helm "${helm_arg}"
      elif [ ${array_target[$target_id-1]} == "ciliumcli" ]; then
        print::msg "all" "warning" "开始执行获取cilium操作..."
        get_user_define_softs_version_logic "ciliumcli" "${cilium_cli_ver}"
        cilium_cli_arg="${soft_version}"
        get_offline_cilium_cli "${cilium_cli_arg}"
      elif [ ${array_target[$target_id-1]} == "kernel" ]; then
        print::msg "all" "warning" "开始执行获取kernel操作..."
        get_offline_centos_kernel "${kernel_version}"
      elif [ ${array_target[$target_id-1]} == "退出" ]; then
        print::msg "all" "warning" "开始执行离线文件管理退出操作..."
        exit
      fi
    else
      Usage
    fi
  done
}

# 调用区域
main
