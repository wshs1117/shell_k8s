#!/bin/bash
# *************************************
# 功能: Shell脚本模板
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-07-13
# *************************************

input_num="$1"

if [[ "${input_num}" =~ ^[0-9]+$ ]]; then
  echo "数字" 
elif [[ "${input_num}" =~ ^[0-9a-Z]+$ ]]; then
  echo "字符串"
else
  echo "其他"
fi
