#!/bin/bash
# *************************************
# 功能: 相关离线文件的获取
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2023-12-20
# *************************************

# 离线目录的创建
offline_dir_create(){
  # 创建离线文件目录
  file_oper_logic "local" "create" "dir"  "${images_dir}"

  # 创建expect目录
  file_oper_logic "local" "create" "dir"  "${expect_centos_dir}"
  file_oper_logic "local" "create" "dir"  "${expect_ubuntu_dir}"

  # 创建docker-ce目录
  file_oper_logic "local" "create" "dir"  "${docker_dir}"

  # 创建cri-dockerd目录
  file_oper_logic "local" "create" "dir"  "${cri_dockerd_dir}"
  
  # 创建harbor目录
  file_oper_logic "local" "create" "dir"  "${harbor_dir}"
  
  # 创建compose目录
  file_oper_logic "local" "create" "dir"  "${compose_dir}"

  # 创建kubernetes目录
  file_oper_logic "local" "create" "dir"  "${k8s_centos_dir}"
  file_oper_logic "local" "create" "dir"  "${k8s_ubuntu_dir}"
 
  # 创建helm目录
  file_oper_logic "local" "create" "dir"  "${helm_dir}"

  # 创建cilium cli目录
  file_oper_logic "local" "create" "dir"  "${cilium_cli_dir}"
  
  # 创建centos的高版本内核目录
  file_oper_logic "local" "create" "dir"  "${kernel_centos_dir}"

  # 创建containerd目录
  file_oper_logic "local" "create" "dir"  "${containerd_dir}"
  file_oper_logic "local" "create" "dir"  "${libsec_dir}" 
  # 创建nerdctl目录
  file_oper_logic "local" "create" "dir"  "${nerdctl_dir}"

  # 创建nginx目录
  file_oper_logic "local" "create" "dir"  "${nginx_dir}"
  
  # 提前获取依赖关系的命令
  [ "${os_type}" == "Ubuntu" ] && "${cmd_type}" install -y apt-rdepends
}

# 获取github软件版本号
get_softs_version_online_from_github(){
  # 接收参数
  local softs_name="$1"
  local github_tags_url="$2"
  local version_simple="$3"

  # 从网站获取文件列表
  get_soft_tags_from_github_logic "${github_tags_url}"
  read -p "请输入您要获取的${softs_name}版本号(示例: ${version_simple} ): " my_ver
}

# 获取github默认版本的软件
get_offline_softs_from_github_default(){
  # 接收参数
  local softs_dir="$1"
  local softs_tar="$2"
  local softs_name="$3"
  local softs_url="$4"

  # 判断离线目录是否正常
  if [ -d "${softs_dir}" ]; then
    # 获取cilium cli离线文件
    if [ -f "${softs_dir}/${softs_tar}" ]; then
      print::msg "console" "warning" "指定版本的${softs_name}离线软件已存在!!!"
    else
      wget "${softs_url}" -O "${softs_dir}/${softs_tar}"
    fi
  else
    print::msg "console" "error" "指定${softs_name}离线文件目录不存在，请先创建目录!!!"
    return
  fi
}

# 定制离线获取expect软件
get_offline_expect(){
  # 根据部署服务器的类型，自动获取软件到特定目录
  if [[ "${os_type}" == "Ubuntu" ]]; then
    cd "${expect_ubuntu_dir}"
    [ -f ${expect_ubuntu_dir}/expect*.deb ] \
         && (print::msg "console" "warning" "expect文件已存在!!!"; return)
    sudo "${cmd_type}" download $(apt-rdepends expect | grep -Ev "^ |debconf")
  else
    [ -f ${expect_centos_dir}/expect*.rpm ] \
         && (print::msg "console" "warning" "expect文件已存在!!!"; return)
    "${cmd_type}" install --downloadonly --downloaddir="${expect_centos_dir}" expect
    rm -f "${expect_centos_dir}/*.yumtx"
  fi
}

# 定制获取离线文件的软件版本逻辑
get_user_define_softs_version_logic(){
  # 接收参数
  local soft_name="$1"
  local default_version="$2"

  # 获取离线文件的软件版本逻辑
  read -p "您是否需要指定获取离线的${soft_name}版本(yes|no): " get_status
  if [ "${get_status}" == "yes" ]; then
    get_${soft_name}_version_online
    soft_version="${my_ver}"
  else
    soft_version="${default_version}"
  fi
}

# 定制离线获取docker软件
get_offline_docker_default(){
  # 获取nerdctl软件
  get_offline_softs_from_github_default "${docker_dir}" "${docker_tar}" "docker" "${docker_url}"
}

# 获取docker软件包的最新列表
get_docker_version_online(){
  # 从docker网站获取文件列表
  get_softs_version_online_from_github "docker" "${docker_site_url}" "${docker_ver}"
}

# 获取任意版本docker软件
get_offline_docker(){
  local linshi_arg="$1"
  # 如果手工传入的软件版本与默认版本不一致，那么就 重新定制相关环境变量
  if [ "${linshi_arg}" != "${docker_ver}" ]; then
    # 使用定制的containerd版本文件
    local docker_ver="${linshi_arg}"
    local docker_tar="docker-${docker_ver}.tgz"
    local docker_dir="${softs_dir}/docker-ce/${docker_ver}"
    local docker_url="${docker_site_url}/${docker_tar}"
  fi

  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${docker_dir}"
  get_offline_docker_default
}

# 定制离线获取cri-dockerd软件
get_offline_cridockerd_default(){
  # 获取cri-dockerd软件
  get_offline_softs_from_github_default "${cri_dockerd_dir}" "${cri_softs_name}" "cri-dockerd" "${cri_softs_url}"
}

# 获取cri-dockerd软件
get_cridockerd_version_online(){
  # cri-dockerd网站获取文件列表
  get_softs_version_online_from_github "cri-dockerd" "${cri_github_tags}" "${cri_dockerd_ver}"
}

# 获取任意版本cri-dockerd软件
get_offline_cridockerd(){
  local linshi_arg="$1"
  
  # 如果手工传入的软件版本与默认版本不一致，那么就 重新定制相关环境变量
  if [ "${linshi_arg}" != "${cri_dockerd_ver}" ]; then
    # 使用定制的cri_dockerd版本文件
    local cri_dockerd_ver="${linshi_arg}"
    local cri_softs_name="${cri_name}-${cri_dockerd_ver}.amd64.tgz"
    local cri_softs_url="${cri_softs_site}/v${cri_dockerd_ver}/${cri_softs_name}"
    local cri_dockerd_dir="${softs_dir}/cri_dockerd/${cri_dockerd_ver}"
  fi

  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${cri_dockerd_dir}"
  get_offline_cridockerd_default
}


# 获取默认版本的harbor软件
get_offline_harbor_default(){
  # 获取harbor软件
  get_offline_softs_from_github_default "${harbor_dir}" "${harbor_softs}" "harbor" "${harbor_softs_url}"
}

# 获取harbor软件
get_harbor_version_online(){
  # 从harbor网站获取文件列表
  get_softs_version_online_from_github "harbor" "${harbor_github_tags}" "${harbor_version}"
}

# 获取任意版本harbor软件
get_offline_harbor(){
  local linshi_arg="$1"

  # 如果手工传入的软件版本与默认版本不一致，那么就 重新定制相关环境变量
  if [ "${linshi_arg}" != "${harbor_version}" ]; then
    # 使用定制的harbor版本文件
    local harbor_version="${linshi_arg}"
    local harbor_softs="harbor-offline-installer-${harbor_version}.tgz"
    local harbor_softs_url="${harbor_site}/${harbor_version}/${harbor_softs}"
    local harbor_dir="${softs_dir}/harbor/${harbor_version}"
  fi

  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${harbor_dir}"
  get_offline_harbor_default
}

# 获取默认版本的compose软件
get_offline_compose_default(){
  # 获取docker compose软件
  get_offline_softs_from_github_default "${compose_dir}" "${compose_file_name}" "docker-compose" "${compose_softs_site}"
}

# 获取compose软件
get_compose_version_online(){
  # 从compose网站获取文件列表
  get_softs_version_online_from_github "docker-compose" "${compose_github_tags}" "${compose_version}"
}

# 获取任意版本docker-compose软件
get_offline_compose(){
  local linshi_arg="$1"
  # 如果手工传入的软件版本与默认版本不一致，那么就 重新定制相关环境变量
  if [ "${linshi_arg}" != "${compose_version}" ]; then
    # 使用定制的docker-compose版本文件
    local compose_version="${linshi_arg}"
    local compose_softs_site="${compose_github_url}/releases/download/${compose_version}/${compose_file_name}"
    local compose_dir="${softs_dir}/compose/${compose_version}"
  fi

  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${compose_dir}"
  get_offline_compose_default
}

# 获取k8s软件后缀的逻辑函数
get_k8s_softs_tail_tag(){
  # 接收参数
  local k8s_version="$1"
  local k8s_ver_main_num=$(echo "${k8s_version}" | awk -F'.' '{print $2}')

  # Ubuntu系统获取软件信息
  if [ "${os_type}" == "Ubuntu" ]; then
    local k8s_soft_ver_name=$(apt-cache madison kubeadm | grep ${k8s_version} | awk '{print $3}')
    local k8s_soft_tail_name="=${k8s_soft_ver_name}"
  # CentOS系统获取软件信息  
  elif [ "${os_type}" == "CentOS" ]; then
    if [ ${k8s_ver_main_num} -ge 28 ]; then
      local k8s_soft_tail_name='-'${version}'-150500.1.1'
    else
      local k8s_soft_tail_name='-'${version}'-0'
    fi
  fi

  # 返回软件后缀信息
  echo "${k8s_soft_tail_name}"
}

# ubuntu系统下载离线软件
ubuntu_get_offline_k8s_logic(){
  # 接收参数
  local k8s_version="$1"

  # 定义依赖环境变量
  local tail_name=$(get_k8s_softs_tail_tag "${k8s_version}")
  local k8s_softs_list="kubeadm${tail_name} kubelet${tail_name} kubectl${tail_name}"

  # 确保软件存在目录是由下载权限的
  chown _apt "${k8s_ubuntu_dir}"
  cd "${k8s_ubuntu_dir}"

  # 下载最新版本软件及其依赖软件
  sudo "${cmd_type}" download $(apt-rdepends kubeadm kubelet kubectl | grep -Ev "^ |debconf")

  # 删除最新版本的软件
  rm -f kube{adm,ctl,let}_*.deb

  # 下载指定版本的软件
  sudo "${cmd_type}" download ${k8s_softs_list}  
}

# Centos系统下载离线软件
centos_get_offline_k8s_logic(){
  # 接收参数
  local k8s_version="$1"
  
  # 定义依赖环境变量
  local tail_name=$(get_k8s_softs_tail_tag "${k8s_version}")
  local k8s_softs_list="kubeadm${tail_name} kubelet${tail_name} kubectl${tail_name}"

  # 下载指定版本的软件
  "${cmd_type}" install --downloadonly --downloaddir="${k8s_centos_dir}" ${k8s_softs_list}
  rm -f "${k8s_centos_dir}/*.yumtx"
}

# 获取kubernetes默认版本的软件
get_offline_k8s_default(){
  # 接收参数
  local k8s_version="$1"
  
  # 根据部署服务器的类型，自动获取软件到特定目录
  if [[ "${os_type}" == "Ubuntu" ]]; then
    # 检测下载权限文件
    if [ -f ${k8s_ubuntu_dir}/kubeadm*.deb ]; then
       (print::msg "console" "warning" "kubernetes相关文件已存在!!!"; return)
    else
      # 下载离线文件
      ubuntu_get_offline_k8s_logic "${k8s_version}"
    fi
  else
    # 这里的思路，暂时是有问题的 -- 部署服务器和k8s集群必须一样。
    if [ -f ${k8s_centos_dir}/*kubeadm*.rpm ]; then
      print::msg "console" "warning" "kubernetes相关文件已存在!!!"
    else
      # 下载离线文件
      centos_get_offline_k8s_logic
    fi
  fi
  print::msg "console" "success" "获取${k8s_version}版本的kubernetes软件成功"
}


# 格式化显示版本信息
print_k8s_version_sub_list(){
  # 接收参数
  local k8s_version_new_url="$1"
  local main_ver="$2"
  
  # 定制依赖环境变量
  local k8s_version_new_all_url="${k8s_version_new_url}/v${main_ver}/deb/amd64"
  
  # 获取版本列表
  local k8s_softs_list=$(curl -s ${k8s_version_new_all_url} \
                         | awk -F'_|-' '/kubeadm/{print $2}' \
                         | sort -rnut'.' -k 3 | head -6)

  # 格式化输出
  local version_list=""
  for ver in ${k8s_softs_list}; do
    version_list="${ver} ${version_list}"
  done
  echo "${main_ver}系列: ${version_list}"
}

print_k8s_version_list(){
  # 定制依赖环境变量
  local k8s_version_new_url="${k8s_sources_repo_addr}/kubernetes-new/core/stable"
  
  # 获取主版本列表
  local k8s_softs_main_ver_list=$(curl -s ${k8s_version_new_url} \
                                  | awk -F'v|/' '/v1./{print $2}' \
                                  | sort -r | head -6)
  # 格式化输出
  for main_ver in ${k8s_softs_main_ver_list}; do
    print_k8s_version_sub_list "${k8s_version_new_url}" "${main_ver}" 
  done
}

# k8s软件版本获取函数
get_k8s_version(){
  # 老版本地址获取方法
  # local k8s_version_old_url="${k8s_sources_repo_addr}/kubernetes/apt/pool"
  # kubeadm_version_tags="${k8s_sources_repo_addr}/kubernetes/apt/pool/"
  # curl --connect-timeout 10 -s "${kubeadm_version_tags}" | \
  #      awk -F'_|-' '/kubeadm/{print $2}' | grep '1.2' | \
  #      sort -rut'.' --parallel=2 | head -n10
  
  # 定义环境变量
  local k8s_version_new_url="${k8s_sources_repo_addr}/kubernetes-new/core/stable"
  local k8s_version_new_all_url="${k8s_version_new_url}/${main_ver}/"

  # 到远程主机执行获取版本的命令
  print::msg "console" "warning" "kubernetes软件版本获取时间可能长一些，请耐心等待..."
  echo -e "\e[32m当前环境下可安装的kubernetes版本: \e[0m"
  echo "========================================================="
  print_k8s_version_list
  echo "========================================================="
  echo ""
  read -p "请输入您要获取的kubeadm版本号(示例: 1.28.2 ): " my_ver
}

# 获取kubernetes指定版本的软件
get_offline_k8s(){
  # 接收参数
  # 参数示例：yes | no
  local get_k8s_type="$1"

  # 生成k8s软件源文件
  k8s_create_repo_logic
  
  # 执行获取离线文件
  if [ "${get_k8s_type}" == "yes" ]; then
    # 选择要获取的k8s版本
    get_k8s_version  # - 此处的函数需要调整
    # 使用定制的kubernetes版本文件
    local k8s_version="${my_ver}"
    local k8s_dir="${softs_dir}/kubernetes/${k8s_version}"
    local k8s_centos_dir="${k8s_dir}/centos"
    local k8s_ubuntu_dir="${k8s_dir}/ubuntu"
  fi
  
  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${k8s_centos_dir}"
  file_oper_logic "local" "create" "dir" "${k8s_ubuntu_dir}"
  deploy_host_apply_k8s_repo "${k8s_version}"
  get_offline_k8s_default "${k8s_version}"
}



# 获取helm软件
get_helm_version_online(){
  # helm网站获取文件列表
  get_softs_version_online_from_github "helm" "${helm_github_tags}" "${helm_ver}"
}

# 获取helm默认版本的软件
get_offline_helm_default(){
  # 获取helm软件
  get_offline_softs_from_github_default "${helm_dir}" "${helm_tar}" "helm" "${helm_url}"
}
# 获取helm指定版本的软件
get_offline_helm(){
  # 接收参数
  local linshi_arg="$1"
  
  # 获取软件逻辑
  if [ "${linshi_arg}" != "${helm_ver}" ]; then
    # 使用定制的helm版本文件
    local helm_ver="${linshi_arg}"
    local helm_tar="helm-v${helm_ver}-linux-amd64.tar.gz"
    local helm_url="https://get.helm.sh/${helm_tar}"
    local helm_dir="${softs_dir}/helm/${helm_ver}"
  fi

  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${helm_dir}"
  get_offline_helm_default
}

# 获取cilium cli软件
get_ciliumcli_version_online(){
  # cilium cli网站获取文件列表
  get_softs_version_online_from_github "ciliumcli" "${cilium_cli_github_tags}" "${cilium_cli_ver}"
}

# 获取cilium cli默认版本的软件
get_offline_cilium_cli_default(){
  # 获取cilium cli软件
  get_offline_softs_from_github_default "${cilium_cli_dir}" "${cilium_cli_tar}" "CiliumCli" "${cilium_cli_url}"
}

# 获取cilium 指定版本的软件
get_offline_cilium_cli(){
  # 接收参数
  local linshi_arg="$1"

  # 获取软件逻辑
  if [ "${linshi_arg}" != "${cilium_cli_ver}" ]; then
    # 使用定制的cilium cli版本文件
    local cilium_cli_ver="${linshi_arg}"
    local cilium_cli_dir="${softs_dir}/cilium_cli/${cilium_cli_ver}"
    local cilium_cli_url="${cilium_cli_github_base}/releases/download/v${cilium_cli_ver}/${cilium_cli_tar}"
  fi
  
  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${cilium_cli_dir}"
  get_offline_cilium_cli_default
}

# 定制获取centos7的默认版本的内核软件
get_offline_centos_kernel_default(){
  # 接收参数
  local kernel_version="$1"

  # 开始获取
  for i in kernel-ml kernel-ml-devel; do
    local kernel_name="${i}-${kernel_version}-${kernel_tail}"
    local softs_url="${kernel_url}/${kernel_name}"
    if [ ! -f "${kernel_centos_dir}/${kernel_name}" ]; then
      wget ${softs_url} -O "${kernel_centos_dir}/${kernel_name}"
    fi
  done
}

# 获取centos7的高版本内核软件
get_offline_centos_kernel(){
   # 接收参数
   local kernel_version="$1"
   
   # 创建基础目录
   file_oper_logic "local" "create" "dir" "${kernel_centos_dir}"

   # 获取制定版本的内核软件 
   get_offline_centos_kernel_default "${kernel_version}"
} 

# 获取containerd软件
get_containerd_version_online(){
  # containerd网站获取文件列表
  get_softs_version_online_from_github "Containerd" "${containerd_github_tags}" "${containerd_ver}"
}

# 获取containerd默认版本的软件
get_offline_containerd_default(){
  # 获取containerd软件
  get_offline_softs_from_github_default "${containerd_dir}" "${containerd_tar}" "Containerd" "${containerd_url}"
}

# 获取libseccomp默认版本的软件
get_offline_libseccomp_default(){
  # 获取libseccomp软件
  file_oper_logic "local" "create" "dir" "${libsec_dir}"
  get_offline_softs_from_github_default "${libsec_dir}" "${libsec_tar}" "libseccomp" "${libsec_url}"
}

# 获取containerd指定版本的软件
get_offline_containerd(){
  # 接收参数
  local linshi_arg="$1"

  # 如果手工传入的软件版本与默认版本不一致，那么就 重新定制相关环境变量
  if [ "${linshi_arg}" != "${containerd_ver}" ]; then
    # 使用定制的containerd版本文件
    local containerd_ver="${linshi_arg}"
    local containerd_tar="cri-containerd-cni-${containerd_ver}-linux-amd64.tar.gz"
    local containerd_dir="${softs_dir}/containerd/${containerd_ver}"
    local containerd_url="${containerd_github_url}/releases/download/v${containerd_ver}/${containerd_tar}"
  fi

  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${containerd_dir}"
  get_offline_containerd_default
  get_offline_libseccomp_default  
}

# 获取nerdctl软件
get_nerdctl_version_online(){
  # nerdctl网站获取文件列表
  get_softs_version_online_from_github "nerdctl" "${nerdctl_github_tags}" "${nerdctl_ver}"
}

# 获取nerdctl默认版本的软件
get_offline_nerdctl_default(){
  # 获取nerdctl软件
  get_offline_softs_from_github_default "${nerdctl_dir}" "${nerdctl_tar}" "nerdctl" "${nerdctl_url}"
}

# 获取containerd指定版本的软件
get_offline_nerdctl(){
  # 接收参数
  local linshi_arg="$1"

  # 如果手工传入的软件版本与默认版本不一致，那么就 重新定制相关环境变量
  if [ "${linshi_arg}" != "${nerdctl_ver}" ]; then
    # 使用定制的containerd版本文件
    local nerdctl_ver="${linshi_arg}"
    local nerdctl_tar="nerdctl-full-${nerdctl_ver}-linux-amd64.tar.gz"
    local nerdctl_dir="${softs_dir}/nerdctl/${nerdctl_ver}"
    local nerdctl_url="${nerdctl_github_url}/releases/download/v${nerdctl_ver}/${nerdctl_tar}"
  fi

  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${nerdctl_dir}"
  get_offline_nerdctl_default
}

# 获取nginx软件
get_nginx_version_online(){
  # nginx网站获取文件列表
  get_softs_version_online_from_github "nginx" "${nginx_github_tags}" "${nginx_ver}"
}

# 获取nginx默认版本的软件
get_offline_nginx_default(){
  # 获取nginx软件
  get_offline_softs_from_github_default "${nginx_dir}" "${nginx_tar}" "nginx" "${nginx_url}"
}

# 获取nginx指定版本的软件
get_offline_nginx(){
  # 接收参数
  local linshi_arg="$1"

  # 如果手工传入的软件版本与默认版本不一致，那么就 重新定制相关环境变量
  if [ "${linshi_arg}" != "${nginx_ver}" ]; then
    # 使用定制的containerd版本文件
    local nginx_ver="${linshi_arg}"
    local nginx_tar="nginx-${nginx_ver}.tar.gz"
    local nginx_dir="${softs_dir}/nginx/${nginx_ver}"
    local nginx_url="https://nginx.org/download/${nginx_tar}"
  fi

  # 获取指定版本的软件
  file_oper_logic "local" "create" "dir" "${nginx_dir}"
  get_offline_nginx_default
}

# 

# 
# 在线集群部署自动获取文件
one_key_get_offline_files_online(){
  print::msg "all" "warning" "在线方式部署kubernetes准备获取依赖的离线软件"
  if [ "${default_container_engine_type}" == "docker" ];then
    # 检测cri文件
    get_offline_cridockerd "${cri_dockerd_ver}"
  fi
  
  if [ "${default_container_engine_type}" == "containerd" ];then
    # 检测containerd文件
    get_offline_containerd "${containerd_ver}" 
      if [ "${containerd_build_func}" == "yes" ]; then
        get_offline_nerdctl "${nerdctl_ver}" 
        get_offline_nginx "${nginx_ver}"
      fi
  fi
  
  # 检测harbor文件
  get_offline_harbor "${harbor_version}"

  # 检测网络解决方案
  if [ "${default_network_type}" == "cilium" ]; then
    # 检测helm文件
    get_offline_helm "${helm_ver}"
    # 检测cilium文件
    get_offline_cilium_cli "${cilium_cli_ver}"
    # 检测centos内核文件
    get_offline_centos_kernel "${kernel_version}"
  fi

  print::msg "all" "success" "在线方式部署kubernetes依赖的离线软件获取成功"
}

# 其他软件的一键获取功能函数
one_key_get_offline_container_softs(){
  # 检测docker文件
  get_offline_docker "${docker_ver}"
  # 检测cri文件
  get_offline_cridockerd "${cri_dockerd_ver}"
  # 检测containerd文件
  get_offline_containerd "${containerd_ver}"
  get_offline_nerdctl "${nerdctl_ver}"
}

# 一键获取镜像仓库功能函数
one_key_get_offline_register_softs(){
  # 检测compose文件
  get_offline_compose ${compose_version}
  # 检测harbor文件
  get_offline_harbor "${harbor_version}"
  # 检测nginx文件
  get_offline_nginx "${nginx_ver}"
}

# 一键获取网络解决方案功能函数
one_key_get_offline_network_softs(){
  # 检测helm文件
  get_offline_helm "${helm_ver}"
  # 检测cilium文件
  get_offline_cilium_cli "${cilium_cli_ver}"
  # 检测centos内核文件
  get_offline_centos_kernel "${kernel_version}"
}

# 一键获取云原生软件功能函数
one_key_get_offline_native_softs(){
  echo 获取相关软件
}


# 离线集群部署自动获取文件
one_key_get_offline_files_offline(){
  print::msg "all" "warning" "离线方式部署kubernetes准备获取依赖的离线软件"
  get_offline_expect
  one_key_get_offline_container_softs
  one_key_get_offline_register_softs
  get_offline_k8s_default "${k8s_version}"
  one_key_get_offline_network_softs
  one_key_get_offline_native_softs
  print::msg "all" "success" "离线方式部署kubernetes依赖的离线软件获取成功"
}
 
