#!/bin/bash
# *************************************
# 功能: K8s集群初始化功能脚本
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2023-07-31
# *************************************

# 注意：现阶段，我假设所有的节点类型都是一样的。

# 加载项目的配置属性信息
source_proj_base_conf(){
  # 获取当前项目的根路径
  root_dir=$(dirname $PWD)

  # 确保项目存在基础配置文件
  if [ ! -f $root_dir/conf/config ]; then
    cp $root_dir/tmpl/* $root_dir/conf/
  fi

  # 加载基本环境属性
  source $root_dir/conf/config
}

# 加载子函数
source_proj_base_conf
source ${subfunc_dir}/${base_func_exec}
source ${subfunc_dir}/${base_func_menu}
source ${subfunc_dir}/${base_func_usage}
source ${subfunc_dir}/${k8s_func_exec}
source ${subfunc_dir}/${base_func_image}
source ${subfunc_dir}/${k8s_subfunc_network}


# 基础内容
array_target=(一键 软件源 安装 初始化 加入 网络 退出)

# 主函数
main(){
  while true
  do
    # 打印k8s集群初始化菜单
    k8s_init_menu
    read -p "请输入您要操作的选项id值: " target_id
    [ -e ${target_id} ] && target_id='100'
    local num_type=$(user_input_datatype_confirm "${target_id}")
    if [ "${num_type}" != "is_num" ]; then
      print::msg "console" "error" "请输入正确格式的内容信息..."
      continue
    fi
    if [ ${#array_target[@]} -ge ${target_id} ]; then
      if [ ${array_target[$target_id-1]} == "一键" ]; then
         print::msg "all" "warning" "开始执行一键k8s集群初始化操作..."
         k_install_type=$(user_define_xxx_type "一键部署k8s集群" "online" "offline" "${default_deploy_type}")
         local_repo_type=$(user_define_xxx_type "是否使用本地harbor镜像仓库" "yes" "no" "${default_repo_type}")
         onekey_cluster_init "${k_install_type}" "${local_repo_type}"
      elif [ ${array_target[$target_id-1]} == "软件源" ]; then
         print::msg "all" "warning" "开始执行k8s集群定制软件源操作..."
         ip_list=$(user_define_create_ip_list "k8s集群定制软件源")
         create_repo "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "安装" ]; then
         print::msg "all" "warning" "开始执行k8s集群软件安装操作..."
         k8s_version_list
         k_install_type=$(user_define_xxx_type "部署k8s集群" "online" "offline" "${default_deploy_type}")
         ip_list=$(user_define_create_ip_list "k8s集群软件安装")
         k8s_install "${k_install_type}" "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "初始化" ]; then
         print::msg "all" "warning" "开始执行k8s集群初始化操作..."
         local_repo_type=$(user_define_xxx_type "是否使用本地harbor镜像仓库" "yes" "no" "${default_repo_type}")
         is_get_image=$(user_define_xxx_type "是否提前获取镜像文件" "yes" "no" "${default_get_image_type}")
         is_use_offline_image=$(user_define_xxx_type "是否使用本地离线镜像文件" "yes" "no" "${default_use_image_type}")
         get_images "${local_repo_type}" "${master1}" "${is_get_image}" "${is_use_offline_image}"
         cluster_create "${local_repo}"
         k8s_master_tail 
      elif [ ${array_target[$target_id-1]} == "加入" ]; then
         print::msg "all" "warning" "开始执行node加入K8s集群操作..."
         host_role=$(user_define_xxx_type "增加的k8s节点角色" "master" "node" "node")
         ip_list=$(user_define_create_ip_list "增加k8s节点")
         add_k8s_node "${host_role}" "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "网络" ]; then
         print::msg "all" "warning" "开始执行定制K8s集群网络解决方案操作..."
         network_type=$(user_define_xxx_type "网络解决方案部署" "flannel" "calico" "cilium" "${default_network_type}")
         k8s_network_install "${network_type}"
      elif [ ${array_target[$target_id-1]} == "退出" ]; then
         print::msg "all" "warning" "开始执行K8s集群初始化退出操作..."
         exit
      fi
    else
      Usage
    fi
  done
}

# 执行主函数
main
