#!/bin/bash
# *************************************
# 功能: Shell脚本模板
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-07-12
# *************************************

ver_num_main=$(echo "$*" | awk '{print $NF}')

update_ver_name=$(echo "$*" | awk '{print $(NF-1)}')

num_list=$(echo "$*" | awk '{$NF=null; $(NF-1)=null; print $0}')

echo $ver_num_main

echo $update_ver_name

echo $num_list
