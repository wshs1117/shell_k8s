#!/bin/bash
# *************************************
# 功能: Shell自动化运维k8s项目总入口
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-07-24
# *************************************


# 加载项目的配置属性信息
root_dir=$(dirname $PWD)
[ -f $root_dir/tmpl/config ] && source $root_dir/tmpl/config || exit

# 加载功能函数
source ${subfunc_dir}/${base_func_menu}
source ${subfunc_dir}/${base_func_usage}
source ${subfunc_dir}/${base_func_exec}
source ${subfunc_dir}/${base_func_conf}

# 基础内容
array_target=(配置 部署 退出)

# 主函数区域
main(){
  while true
  do
    project_menu
    read -p "请输入您要操作的选项id值: " target_id
    # bug修复：避免target_id为空的时候，条件判断有误
    [ -e ${target_id} ] && target_id='100'
    # bug修复: 避免输入非数字内容
    local num_type=$(user_input_datatype_confirm "${target_id}")
    if [ "${num_type}" != "is_num" ]; then
      print::msg "console" "error" "请输入正确格式的内容信息..."
      continue
    fi

    if [ ${#array_target[@]} -ge ${target_id} ]; then
      # 增加一键功能执行
      if [ ${array_target[$target_id-1]} == "配置" ]
      then
        print::msg "all" "warning" "开始执行自动调整项目部署属性的操作..."
        one_key_edit_project_config
      elif [ ${array_target[$target_id-1]} == "部署" ]
      then
        print::msg "all" "warning" "开始执行shell管理K8s环境操作..."
        bash_local_script_file "${k8s_deploy_scripts}"
      elif [ ${array_target[$target_id-1]} == "退出" ]
      then
        print::msg "all" "warning" "开始执行Shell自动化运维K8s退出操作..."
  exit
      fi
    else
      Usage
    fi
  done
}

# 调用区域
main
