#!/bin/bash
# *************************************
# 功能: 获取函数的多返回值
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-07-19
# *************************************


func1(){
  local num="$1"
  echo "value1" "value2" "${num}"
}

func2(){
  read key1 key2 key3 < <(func1 "23")
  echo $key1
  echo $key2
  echo $key3
}

func2

