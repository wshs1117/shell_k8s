#!/bin/bash
# *************************************
# 功能: 通用传输文件方法
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-09-04
# *************************************

# 远程传输文件通用功能函数
scp_file_base_func(){
  # 接收参数
  local file_type="$1"
  local tdir=$(echo $* | awk '{print $NF}')
  local sfile=$(echo $* | awk '{print $(NF-1)}')
  local ip_list=$(echo $* | awk '{$NF=null;$(NF-1)=null;$1=null;print $0}')

  # 判断文件传输
  if [ "${file_type}" == "file" ]; then
    local scp_options=""
  elif [ "${file_type}" == "dir" ]; then
    local scp_options="-r"
  fi

  # 传输远程文件
  for ip in ${ip_list}; do
    # 注意：${scp_options} 不允许外侧有双引号(")
    scp ${scp_options} "${sfile}" "${login_user}@${ip}:${tdir}"
  done
}

scp_file(){
  local file="/etc/passwd"
  scp_file_base_func "file" "10.0.0.17" "${file}" "/tmp"
}

scp_dir(){
  local dir="/boot/grub"
  scp_file_base_func "dir" "10.0.0.17" "${dir}" "/tmp"
  # 注意：这里传递的目标目录是 grub
}

scp_dir2(){
  local dir="/usr/local/bin"
  scp_file_base_func "dir" "10.0.0.17" "${dir}" "/usr/local"
}
scp_dir2
