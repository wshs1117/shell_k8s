#!/bin/bash

# 单分支if语句
if
then
fi

# 双分支if语句 
if
then
else
fi

# 多分支if语句
if
then
elif
then
elif
then
else
fi

# 多分支内容匹配
case i in 
  "值1")
	  xxxx;;
  "值2")
	  xxxx;;
  *)
	  xxx;;
esac 
# case 主要的应用场景  systemctl restart|start|stop|reload|... service_name
