#!/bin/bash
# *************************************
# 功能: Shell 部署 K8s的入口脚本文件
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2023-04-13
# *************************************

# 加载项目的配置属性信息
source_proj_base_conf(){
  # 获取当前项目的根路径
  root_dir=$(dirname $PWD)
  
  # 确保项目存在基础配置文件
  if [ ! -f $root_dir/conf/config ]; then
    cp $root_dir/tmpl/* $root_dir/conf/
  fi

  # 加载基本环境属性
  source $root_dir/conf/config
}

# 加载功能函数
source_proj_base_conf
source ${subfunc_dir}/${base_func_menu}
source ${subfunc_dir}/${base_func_usage}
source ${subfunc_dir}/${base_func_exec}
source ${subfunc_dir}/${base_func_secure}
source ${subfunc_dir}/${base_func_softs}
source ${subfunc_dir}/${base_func_offline}
source ${subfunc_dir}/${base_func_image}
source ${subfunc_dir}/${base_cluster_exec}
source ${subfunc_dir}/${k8s_func_exec}
source ${subfunc_dir}/${k8s_cluster_deploy}
source ${subfunc_dir}/${k8s_subfunc_network}

# 基础内容
array_target=(一键 基础 集群 初始化 管理 清理 网络 存储 平台 资源 离线 退出)

# 主函数区域
main(){
  while true
  do
    manager_menu
    read -p "请输入您要操作的选项id值: " target_id
    # bug修复：避免target_id为空的时候，条件判断有误
    [ -e ${target_id} ] && target_id='100'
    # bug修复: 避免输入非数字内容
    local num_type=$(user_input_datatype_confirm "${target_id}")
    if [ "${num_type}" != "is_num" ]; then
      print::msg "console" "error" "请输入正确格式的内容信息..."
      continue
    fi

    if [ ${#array_target[@]} -ge ${target_id} ]; then
      # 增加一键功能执行
      if [ ${array_target[$target_id-1]} == "一键" ]; then
        print::msg "all" "warning" "开始执行一键Kubernetes集群部署操作..."
        one_key_k8s_cluster_deploy
      elif [ ${array_target[$target_id-1]} == "基础" ]; then
        print::msg "all" "warning" "开始执行通用基础环境部署操作..."
        bash_local_script_file "${base_env_scripts}"
      elif [ ${array_target[$target_id-1]} == "集群" ]; then
        print::msg "all" "warning" "开始执行K8s集群基础环境部署操作..."
        bash_local_script_file "${base_cluster_scripts}"
      elif [ ${array_target[$target_id-1]} == "初始化" ]; then
        print::msg "all" "warning" "开始执行K8s环境初始化操作...\e[0m"
        bash_local_script_file "${k8s_cluster_init_scripts}"
      elif [ ${array_target[$target_id-1]} == "管理" ]; then
        print::msg "all" "warning" "开始执行K8s集群功能管理操作..."
        bash_local_script_file "${k8s_cluster_function_manager_scripts}"
      elif [ ${array_target[$target_id-1]} == "清理" ]; then
        print::msg "all" "warning" "开始执行k8s集群清理管理操作..."
        bash_local_script_file "${k8s_cluster_clean_manager_scripts}"
      elif [ ${array_target[$target_id-1]} == "网络" ]; then
        print::msg "all" "warning" "开始执行k8s集群网络管理操作..."
        bash_local_script_file "${k8s_cluster_network_manager_scripts}"
      elif [ ${array_target[$target_id-1]} == "存储" ]; then
        print::msg "all" "warning" "开始执行k8s集群存储管理操作..."
        bash_local_script_file "${k8s_cluster_storage_manager_scripts}"
      elif [ ${array_target[$target_id-1]} == "平台" ]; then
        print::msg "all" "warning" "开始执行k8s集群平台基础环境管理操作..."
        bash_local_script_file "${k8s_cluster_depend_manager_scripts}"
      elif [ ${array_target[$target_id-1]} == "资源" ]; then
        print::msg "all" "warning" "开始执行k8s集群资源对象管理操作..."
        bash_local_script_file "${k8s_cluster_resource_manager_scripts}"
      elif [ ${array_target[$target_id-1]} == "离线" ]; then
        print::msg "all" "warning" "开始执行离线方式获取K8s集群的软件操作..."
        bash_local_script_file "${kubernetes_offline_prepare}"
      elif [ ${array_target[$target_id-1]} == "退出" ]; then
        print::msg "all" "warning" "开始执行K8S集群管理退出操作..."
        exit
      fi
    else
      Usage
    fi
  done
}

# 调用区域
main

