#!/bin/bash
# *************************************
# 功能: Shell脚本模板
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-08-11
# *************************************

# 环境变量
github_domain="githubfast.com"
helm_version="3.15.0"
helm_github_url="https://${github_domain}/helm/helm"
helm_github_tags="${helm_github_url}/tags"

# 从github中获取软件的版本信息
get_soft_tags_from_github_logic(){
  # 接收参数
  local softs_tags_url="$1"

  # 获取tag信息
  echo "--------------------------------------------------------"
  curl --connect-timeout 10 -s "${softs_tags_url}" | awk -F'>|<' '/h2 data/{print $(NF-4)}' | grep -v 'rc'
  echo "--------------------------------------------------------"
}

# 获取helm软件
get_helm_version_online(){
  # helmr网站获取文件列表
  get_soft_tags_from_github_logic "${helm_github_tags}"
  read -p "请输入您要获取的helm版本号(示例: 3.15.0 ): " my_ver
}

# 定制获取离线文件的软件版本逻辑
get_user_define_softs_version_logic(){
  # 接收参数
  local soft_name="$1"
  local default_version="$2"

  # 获取离线文件的软件版本逻辑
  read -p "您是否需要指定获取离线的${soft_name}版本(yes|no): " get_status
  if [ "${get_status}" == "yes" ]; then
    get_${soft_name}_version_online
    soft_version="${my_ver}"
  else
    soft_version="${default_version}"
  fi
}



main(){
  get_user_define_softs_version_logic "helm" "${helm_version}"
  helm_arg="${soft_version}"
  echo "get_offline_helm ${helm_arg}"
}

main
