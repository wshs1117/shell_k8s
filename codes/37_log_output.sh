#!/bin/bash
# *************************************
# 功能: Shell 脚本信息输出
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-07-18
# *************************************

# 定制基础变量
log_file='/tmp/deploy.log'
log_info="正常"
log_succ="成功"
log_warn="提示"
log_erro="异常"

#  echo -e "\e[33m开始执行离线方式部署K8s集群的基础操作...\e[0m"

# 定制正常信息
log::info(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;37m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_info} | tee -a ${log_file}
}

# 定制成功信息
log::success(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;32m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_succ} | tee -a ${log_file}
}

# 定制提示信息
log::warning(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;33m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_warn} | tee -a ${log_file}
}

# 定制错误信息
log::error(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;31m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_erro} | tee -a ${log_file}
}

log::info "dsfadsaf fdsaf fdasfds"
log::success "dsfadsaf fdsaf fdasfds"
log::warning "dsfadsaf fdsaf fdasfds"
log::error "dsfadsaf fdsaf fdasfds"
