#!/bin/bash
# *************************************
# 功能: 主功能函数所依赖的菜单函数
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2023-04-13
# *************************************

# 项目入口菜单函数
project_menu(){
  echo -e "\033[31m               Shell自动化运维K8s入口\033[0m"
  echo -e "\033[32m===========================================================\033[0m"
  echo -e "\033[32m1: 配置项目属性     2: 管理K8s环境       3: 退出操作\033[0m"
  echo -e "\033[32m===========================================================\033[0m"
}

# 主脚本菜单函数
manager_menu(){
  echo -e "\033[31m           Shell自动化运维K8s管理平台\033[0m"
  echo -e "\033[32m=============================================================\033[0m"
  echo -e "\033[32m 1: 一键集群部署     2: 通用基础环境       3: 集群基础环境\033[0m"
  echo -e "\033[32m 4: K8s集群初始化    5: K8s集群功能管理    6: K8s集群清理管理\033[0m"
  echo -e "\033[32m 7: K8s网络方案管理  8: K8s存储方案管理    9: K8s平台基础功能\033[0m"
  echo -e "\033[32m10: K8s资源对象管理 11: K8s离线环境准备   12: 退出操作\033[0m"
  echo -e "\033[32m=============================================================\033[0m"
}


# 基础环境操作菜单函数
base_env_menu(){
  echo -e "\033[31m        通用基础环境管理界面\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
  echo -e "\033[32m1: 一键全局环境     2: 基本环境部署    3: 跨主机免密码认证\033[0m"
  echo -e "\033[32m4: 同步集群hosts    5: 设定集群主机名  6: 更新软件源\033[0m"
  echo -e "\033[32m7: 一键主机环境     8: 退出操作\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
}

# 集群基础环境操作菜单函数
base_cluster_menu(){
  echo -e "\033[31m        集群基础环境管理界面\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
  echo -e "\033[32m1: 一键集群环境     2: K8s内核参数配置  3: Docker环境部署\033[0m"
  echo -e "\033[32m4: Containerd环境   5: CRI-O环境部署    6: Harbor环境\033[0m"
  echo -e "\033[32m7: Keepalived环境   8: Haproxy代理环境  9: Nginx代理环境\033[0m"
  echo -e "\033[32m10: 退出操作\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
}

# k8s集群初始化操作菜单函数
k8s_init_menu(){
  echo -e "\033[31m    Kubernetes集群初始化管理界面\033[0m"
  echo -e "\033[32m============================================\033[0m"
  echo -e "\033[32m1: 一键部署环境     2: 定制软件源\033[0m"
  echo -e "\033[32m3: 应用软件安装     4: 集群初始化\033[0m"
  echo -e "\033[32m5: 节点加入集群     6: 定制网络操作\033[0m"
  echo -e "\033[32m7: 退出操作\033[0m"
  echo -e "\033[32m============================================\033[0m"
}

# k8s集群平台功能实践菜单函数
k8s_addons_menu(){
  echo -e "\033[31m    Kubernetes平台功能管理界面\033[0m"
  echo -e "\033[32m============================================\033[0m"
  echo -e "\033[32m1: 可视化环境         2: prometheus监控环境\033[0m"
  echo -e "\033[32m3: ELK日志环境        4: SC数据持久化环境\033[0m"
  echo -e "\033[32m5: Ceph分布式存储环境 6: 退出操作\033[0m"
  echo -e "\033[32m============================================\033[0m"
}


# k8s集群离线准备工作实践菜单函数
k8s_offline_menu(){
  echo -e "\033[31m      Kubernetes离线准备工作管理界面\033[0m"
  echo -e "\033[32m=======================================================\033[0m"
  echo -e "\033[32m 1: 创建目录结构     2: 获取Expect      3: 获取Docker\033[0m"
  echo -e "\033[32m 4: 获取Cri-dockerd  5: 获获Containerd  6: 取取nerdctl\033[0m"
  echo -e "\033[32m 7: 取取Nginx        8: 获取Harbor      9: 获取Compose\033[0m"
  echo -e "\033[32m10: 取取kubernetes  11: 获取Helm       12: 获取CiliumCli\033[0m"
  echo -e "\033[32m13: 获取kernel      14: 退出操作\033[0m"
  echo -e "\033[32m=======================================================\033[0m"
}

# k8s集群功能管理实践菜单函数
k8s_cluster_function_manager(){
  echo -e "\033[31m              Kubernetes集群功能管理界面\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
  echo -e "\033[32m1: 工作节点扩缩容   2: 控制节点扩缩容   3: 工作节点升级\033[0m"
  echo -e "\033[32m4: 控制节点升级     5: 集群证书升级     6: 集群数据备份\033[0m"
  echo -e "\033[32m7: 集群数据还原     8: 一键集群升级     9: 退出操作\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
}

# k8s集群清理管理实践菜单函数
k8s_cluster_clean_manager(){
  echo -e "\033[31m              Kubernetes集群清理管理界面\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
  echo -e "\033[32m 1: 一键集群清理     2: 一键重置集群     3: 清理集群网络\033[0m"
  echo -e "\033[32m 4: 清理工作节点     5: 清理控制节点     6: 清理容器环境\033[0m"
  echo -e "\033[32m 7: 清理内核环境     8: 清理仓库环境     9: 清理高可用环境\033[0m"
  echo -e "\033[32m10: 清理ssh环境     11: 退出操作\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
}

# K8s集群网络环境管理实践菜单函数
k8s_cluster_network_manager(){
  echo -e "\033[31m            Kubernetes集群网络环境管理界面\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
  echo -e "\033[32m1: Flannel   2: Calico     3: Cilium     4: 退出操作\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
}

# K8s集群存储环境管理实践菜单函数
k8s_cluster_storage_manager(){
  echo -e "\033[31m            Kubernetes集群存储环境管理界面\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
  echo -e "\033[32m1: NFS       2: Ceph       3: Longhorn   4: GlusterFS\033[0m"
  echo -e "\033[32m5: MooseFS   6: HDFS       7: 退出操作\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
}


# K8s集群基础环境管理实践菜单函数
k8s_cluster_depend_manager(){
  echo -e "\033[31m            Kubernetes集群基础环境管理界面\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
  echo -e "\033[32m1: 用户管理  2: 本地DNS   3: 配置检测    4: 镜像检测\033[0m"
  echo -e "\033[32m5: https镜像仓库          6: 退出操作\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
}

# K8s集群资源对象管理实践菜单函数
k8s_cluster_resource_manager(){
  echo -e "\033[31m            Kubernetes集群资源对象管理界面\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
  echo -e "\033[32m1: 资源对象属性     2: 资源对象文件     3: 删除资源对象\033[0m"
  echo -e "\033[32m4: 退出操作\033[0m"
  echo -e "\033[32m==========================================================\033[0m"
}

