#!/bin/bash
# *************************************
# 功能: Containerd环境部署功能脚本
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-09-02
# *************************************


# 基础环境变量
local c_hostlist="$*"
local c_ls_dir="/tmp/containerd"
local c_ls_ca_dir="${c_ls_dir}/cert.d"
local c_config_file=$(basename "${containerd_config}")
local c_hosts_base_dir=$(dirname ${containerd_hosts_dir})
local c_ls_config_file="${c_ls_dir}/${c_config_file}"
local c_ls_context_file="${c_ls_dir}/config_test.txt"

local n_ls_dir="/tmp/nerdctl"
local n_toml=$(echo "${nerdctl_conf##*/}")
local n_ls_toml="${n_ls_dir}/${n_toml}"
local b_ls_dir="/tmp/buildkit"
local b_toml=$(echo "${buildkit_conf##*/}")
local b_ls_toml="${b_ls_dir}/${b_toml}"

local nginx_ls_dir="/tmp/nginx"

# 解压文件功能函数
untar_containerd_file(){
  # 定制环境变量
  local softs_path="${containerd_dir}/${containerd_tar}"
  local softs_name="containerd"

  # 解压文件
  untar_softs_base_func "${softs_name}" "${softs_path}" "${c_ls_dir}"  
}

# 解压文件功能函数
untar_nerdctl_file(){
  # 定制环境变量
  local softs_path="${nerdctl_dir}/${nerdctl_tar}"
  local softs_name="nerdctl"

  # 解压文件
  untar_softs_base_func "${softs_name}" "${softs_path}" "${n_ls_dir}"
}

# 解压containerd环境的文件
untar_containerd_files(){
  # 解压containerd文件
  untar_containerd_file

  # 解压nerdctl文件
  if [ "${containerd_build_func}" == "yes" ]; then
    untar_nerdctl_file
  fi
}

# 创建CA文件
create_containerd_ca_files(){
  # 接收参数
  local ca_dir="$1"
  local ca_name="$2"
  local cn_name="CA"

  # 生成自建CA秘钥
  create_private_key_func "${ca_dir}" "${ca_name}" "${ca_key_num}"

  # 生成自建CA证书
  create_private_ca_cert_func "${ca_dir}" "${ca_name}" "${cn_name}"
}

# 创建harbor服务需要的证书功能函数
create_harbor_ca_files(){
  # 接收参数
  local ca_dir="$1"
  local ca_name="$2"
  local cn_name="$3"

  # 创建harbor主机的CA秘钥
  create_private_key_func "${ca_dir}" "${cn_name}" "${ca_key_num}" 

  # 创建harbor主机的CSR文件
  create_domain_req_csr_func "${ca_dir}" "${cn_name}"

  # 创建专属的域名扩展信息文件
  create_v3crt_bind_domain_func "${ca_dir}" "${cn_name}"

  # 创建harbor的crt证书文件
  create_crt_use_csr_func "${ca_dir}" "${ca_name}" "${cn_name}"

  # 创建harbor的cert证书文件
  create_cert_use_crt_func "${ca_dir}" "${cn_name}"
}

# containerd创建buildkit依赖的镜像仓库证书功能函数
create_buildkit_depend_cert(){
  # 接收参数
  local ca_dir="$1"
  local ca_name="$2"
  local cn_name="$3"

  # 生成harbor专属的证书文件
  if [ "${containerd_build_func}" == "yes" ]; then
    # 创建CA文件依赖的目录
    # create_not_exist_dir_logic "${ca_dir}"
    file_oper_logic "local" "create" "dir" "${ca_dir}"

    # 创建自建类型的CA文件
    if [ "${create_my_ca_type}" == "define" ]; then
      create_containerd_ca_files "${ca_dir}" "${ca_name}"
    fi

    # 基于CA文件生成harbor签名证书
    create_harbor_ca_files "${ca_dir}" "${ca_name}" "${cn_name}"

  fi
}

# 创建配置文件功能函数
create_cntainerd_conf(){
  # 临时环境变量
  local containerd_cmd_path="${c_ls_dir}/usr/local/bin/containerd"

  # 定制Containerd的配置文件
  ${containerd_cmd_path} config default > "${c_ls_config_file}"
  print::msg "console" "success" "创建好containerd的模版配置文件!!!"
}

# containerd 旧版本方式增加tls跳过属性信息功能函数
containerd_hosts_config_edit_tls_old(){
  # 创建tls属性配置
  cat > ${c_ls_context_file} <<-eof
        [plugins."io.containerd.grpc.v1.cri".registry.configs."${harbor_addr}".tls]
          insecure_skip_verify = true
eof
  
  # 完善镜像构建功能属性
  if [ "${containerd_build_func}" == "yes" ]; then
    local ca_attr_value="${c_hosts_base_dir}/${harbor_addr}/${ca_name}.crt"
    echo "          ca_file = \"${ca_attr_value}\"" >> "${c_ls_context_file}"
  fi

  # 设置关键字
  local keyword="registry.configs]"

  # 修改tls属性配置
  sed -i "/${keyword}/r ${c_ls_context_file}" "${c_ls_config_file}"
}

# containerd 旧版本方式增加mirrors属性信息功能函数
containerd_hosts_config_edit_mirrors_old(){
  # 创建mirrors属性配置
  cat > ${c_ls_context_file} <<-eof
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."${harbor_addr}"]
          endpoint = ["${harbor_url}"]
eof

  # 设置关键字
  local keyword="registry.mirrors]"

  # 修改mirrors属性配置
  sed -i "/${keyword}/r ${c_ls_context_file}" "${c_ls_config_file}"
}

# containerd 旧版本方式增加auth属性信息功能函数
containerd_hosts_config_edit_auth_old(){
  # 创建auth属性配置
  cat > ${c_ls_context_file} <<-eof
        [plugins."io.containerd.grpc.v1.cri".registry.configs."${harbor_addr}".auth]
          username = "${harbor_user}"
          password = "${harbor_passwd}"
eof

  # 设置关键字
  local keyword="registry.configs]"

  # 修改mirrors属性配置
  sed -i "/${keyword}/r ${c_ls_context_file}" "${c_ls_config_file}"
}

# containerd 新版本方式增加hosts属性信息功能函数
containerd_hosts_config_edit_hosts_new(){
  # 接受参数
  local domain_name="$1"
  local c_ls_hosts_file="${c_ls_dir}/${domain_name}/${containerd_hosts_file}"
  
  # 创建专属目录
  # create_not_exist_dir_logic "${c_ls_dir}/${domain_name}"
  file_oper_logic "local" "create" "dir" "${c_ls_dir}/${domain_name}"

  # 增加hosts文件
  cat > ${c_ls_hosts_file} <<-eof
server = "${domain_name}"

[host."${domain_name}"]
  capabilities = ["pull", "resolve", "push"]
  skip_verify = true
eof
  
  # 完善构建功能配置
  if [ "${containerd_build_func}" == "yes" ]; then
    echo "  ca = \"${ca_name}.crt\"" >> ${c_ls_hosts_file}
  fi
}

# 定制仓库配置功能函数-旧版
containerd_hosts_config_edit_old(){
  # 增加auth属性
  containerd_hosts_config_edit_auth_old
  
  # 增加tls跳过属性
  containerd_hosts_config_edit_tls_old

  # 增加mirrors属性
  containerd_hosts_config_edit_mirrors_old
  
  # 移除无效文件
  rm -rf "${c_ls_context_file}"
}
 
# 定制仓库配置功能函数-新版
containerd_hosts_config_edit_new(){
  # 启动containerd的新版本hosts配置属性
  local keyword="config_path"
  sed -i "/${keyword}/s@${keyword} = \"@${keyword} = \"${c_hosts_base_dir}@" "${c_ls_config_file}"

  # 增加专属的hosts配置文件
  containerd_hosts_config_edit_hosts_new "${harbor_addr}"
}

# 定制仓库配置功能函数
containerd_hosts_config_edit(){
  # 定制仓库配置
  if [ "${containerd_hosts_config_type}" == "new" ]; then
    containerd_hosts_config_edit_new
  else
    containerd_hosts_config_edit_old
  fi  
}

# 生成buildkit 依赖的配文件
create_buildkit_toml_func(){
  # 接受参数
  local domain_name="$1"

  # 创建配置文件
  # create_not_exist_dir_logic "${b_ls_dir}"
  file_oper_logic "local" "create" "dir" "${b_ls_dir}"

  cat > "${b_ls_toml}"<<-eof
[registry."${domain_name}"]
  http = true
  insecure = true
eof
}

# 生成nerdctld的配置文件
create_nerdctld_toml_func(){
  # 接受参数
  local default_ns="$1"

  # 创建配置文件
  # create_not_exist_dir_logic "${n_ls_dir}"
  file_oper_logic "local" "create" "dir" "${n_ls_dir}"

  cat > "${n_ls_toml}"<<-eof
namespace = "${default_ns}"
debug = false
debug_full = false
insecure_registry = true
eof
}

# 修改buildkit的服务启动文件功能函数
buildkit_service_edit_func(){
  # 接收参数
  local service_dir="lib/systemd/system"
  local service_file="buildkit.service"
  local service_path="${n_ls_dir}/${service_dir}/${service_file}"
  local service_edit="buildkitd --oci-worker=false --containerd-worker=true"

  # 修改配置文件
  if [ "${containerd_build_func}" == "yes" ]; then
    sed -i "/buildkitd$/s@buildkitd.*@${service_edit}@" "${service_path}"
  fi
}

# containerd创建buildkit依赖的配置文件功能函数
create_buildkit_depend_toml(){
  # 接收参数
  local domain_name="$1"
  local default_ns="$2"

  # 创建配置文件
  if [ "${containerd_build_func}" == "yes" ]; then
    create_buildkit_toml_func "${domain_name}"
    create_nerdctld_toml_func "${default_ns}"
  fi
}

# 定制buildkit专属的配置功能函数
buildkit_config_edit(){
  # 定制镜像构建需要的CA文件
  create_buildkit_depend_cert "${c_ls_ca_dir}" "${ca_name}" "${harbor_addr}"

  # 创建buildkit依赖的toml配置文件
  create_buildkit_depend_toml "${harbor_addr}" "${nerdctl_default_namespace}"

  # 修改buildkit的启动服务文件
  buildkit_service_edit_func
}

# 定制全局配置功能函数
containerd_config_edit(){
  # 修改Cgroup的管理方式
  sed -i "/SystemdCgroup/s#false#true#" "${c_ls_config_file}"

  # 修改sandbox的镜像文件地址
  local images_name="${harbor_addr}/${harbor_k8s_repo}/pause:${k8s_pause_image_ver}"
  local keyword="x_image"
  sed -i "/${keyword}/s@${keyword}.*@${keyword} = \"${images_name}\"@" "${c_ls_config_file}"

  # 定制仓库配置
  containerd_hosts_config_edit
  
  # 定制镜像构建需要的相关文件
  buildkit_config_edit  
  print::msg "all" "success" "containerd的配置文件创建成功!!!"
}

# 转移命令功能函数
containerd_scp_cmd_remote(){
  # 接收参数
  local host_addr="$1"
  local c_cmd_dir="usr/local/bin"
  local r_cmd_dir="usr/local/sbin"

  # 传输文件
  local s_c_file="${c_ls_dir}/${c_cmd_dir}" 
  local d_c_dir="/${c_cmd_dir}"
  scp_file_base_func "${host_addr}" "${s_c_file}" "${d_c_dir}"
  local s_r_file="${c_ls_dir}/${r_cmd_dir}"
  local d_r_dir="/${r_cmd_dir}"
  scp_file_base_func "${host_addr}" "${s_r_file}" "${d_r_dir}"
  print::msg "console" "success" "向节点 ${host_addr} 传输containerd的命令文件成功!!!"
}

# 转移cni插件文件功能函数
containerd_scp_plugins_remote(){
  # 接收参数
  local host_addr="$1"
  local plugin_dir="opt/cni"
  
  # 传输文件
  local s_file="${c_ls_dir}/${plugin_dir}"
  local d_dir="/${plugin_dir}"
  #  remote_create_not_exist_dir_logic "${host_addr}" "/${plugin_dir}"
  file_oper_logic "${host_addr}" "create" "dir" "/${plugin_dir}"
  scp_file_base_func "${host_addr}" "${s_file}" "${d_dir}"
  print::msg "console" "success" "向节点 ${host_addr} 传输containerd的cni插件文件成功!!!"
}

# 转移containerd的服务文件功能函数
containerd_scp_service_remote(){
  # 接收参数
  local host_addr="$1"
  local services_dir="etc/systemd/system"

  # 传输文件
  local s_file="${c_ls_dir}/${services_dir}"
  local d_dir="/${services_dir}"
  scp_file_base_func "${host_addr}" "${s_file}" "${d_dir}"
  print::msg "console" "success" "向节点 ${host_addr} 传输containerd的服务文件成功!!!"
}


# 转移containerd配置文件
containerd_scp_config_remote(){
  # 接收参数
  local host_addr="$1"
  local config_dir=$(dirname "${containerd_config}")
  local c_ls_hosts_file="${c_ls_dir}/${harbor_addr}/${containerd_hosts_file}"

  # 创建containerd配置文件目录
  # remote_create_not_exist_dir_logic "${host_addr}" "${config_dir}"
  file_oper_logic "${host_addr}" "create" "dir" "${config_dir}"
  # 传输config.toml配置文件到远程主机
  local s_file="${c_ls_dir}/${services_dir}"
  local d_dir="/${services_dir}"
  scp_file_base_func "${host_addr}" "${c_ls_config_file}" "${config_dir}"

  # 新版containerd传输hosts文件
  if [ "${containerd_hosts_config_type}" == "new" ]; then
    # remote_create_not_exist_dir_logic "${host_addr}" "${containerd_hosts_dir}"
    file_oper_logic "${host_addr}" "create" "dir" "${containerd_hosts_dir}"
    scp_file_base_func "${host_addr}" "${c_ls_hosts_file}" "${containerd_hosts_dir}"
  fi
  print::msg "console" "success" "向节点 ${host_addr} 传输containerd的配置文件成功!!!"
}

# 转移buildkit命令文件
builkdit_scp_cmd_remote(){
  # 接收参数
  local host_addr="$1"
  local n_cmd_dir="bin"

  # 传输文件
  local s_n_file="${n_ls_dir}/${n_cmd_dir}"
  local d_n_dir="/usr/local/${n_cmd_dir}"
  scp_file_base_func "${host_addr}" "${s_n_file}" "${d_n_dir}"
  print::msg "console" "success" "向节点 ${host_addr} 传输builkdit的命令文件成功!!!"
}

# 转移buildkit服务文件
buildkit_scp_service_remote(){
  # 接收参数
  local host_addr="$1"
  local services_dir="lib/systemd/system"
  local service_name="buildkit.service"

  # 传输文件
  local s_file="${n_ls_dir}/${services_dir}/${service_name}"
  local d_dir="/${services_dir}"
  scp_file_base_func "${host_addr}" "${s_file}" "${d_dir}"
  print::msg "console" "success" "向节点 ${host_addr} 传输buildkit的服务文件成功!!!"
}

# 转移buildkit构建文件
buildkit_scp_toml_remote(){
  # 接收参数
  local host_addr="$1"
  local n_config_dir=$(echo "${nerdctl_conf%/*}")
  local b_config_dir=$(echo "${buildkit_conf%/*}")

  # 创建buildkit以及nerdctl配置文件目录
  # remote_create_not_exist_dir_logic "${host_addr}" "${n_config_dir}"
  # remote_create_not_exist_dir_logic "${host_addr}" "${b_config_dir}"
  file_oper_logic "${host_addr}" "create" "dir" "${n_config_dir}" 
  file_oper_logic "${host_addr}" "create" "dir" "${b_config_dir}"

  # 传输文件
  scp_file_base_func "${host_addr}" "${b_ls_toml}" "${b_config_dir}"
  scp_file_base_func "${host_addr}" "${n_ls_toml}" "${n_config_dir}"
  print::msg "console" "success" "向节点 ${host_addr} 传输buildkit的配置文件成功!!!"
}

# 转移buildkit依赖的ca文件功能函数
buildkit_scp_ca_remote(){
  # 接收参数
  local host_addr="$1"
  local ca_name="$2"

  # 传递文件
  local s_file="${c_ls_ca_dir}/${ca_name}.crt"
  local d_dir="$3"
  scp_file_base_func "${host_addr}" "${s_file}" "${d_dir}"  
}

# nerdctl命令的转换功能函数
nerdctl_change_to_docker(){
  # 接收参数
  local host_addr="$1"

  # 转换命令
  local nerdctl_path="/usr/local/bin/nerdctl"
  local docker_path="/usr/local/bin/docker"
  local rm_cmd="[ -f ${docker_path} ] && rm -f ${docker_path}"
  local link_cmd="ln -sv ${nerdctl_path} ${docker_path}"
  remote_node_exec_cmd "${host_addr}" "${rm_cmd}; ${link_cmd}" 
}

# 转移buildkit相关文件到远程主机功能函数
buildkit_scp_files_remote(){
  # 接收参数
  local host_addr="$1"

  # 传递文件
  if [ "${containerd_build_func}" == "yes" ]; then
    # 转移buildkit命令文件
    builkdit_scp_cmd_remote "${host_addr}"
    # 转移buildkit服务文件
    buildkit_scp_service_remote "${host_addr}"
    # 转移buildkit构建文件
    buildkit_scp_toml_remote "${host_addr}"
    # 转移ca相关文件
    buildkit_scp_ca_remote "${host_addr}" "${ca_name}" "${containerd_hosts_dir}"
    # 转换nerdctl命令
    nerdctl_change_to_docker "${host_addr}"
  fi
}

# 转移相关文件到远程主机
containerd_scp_files_remote(){
  # 接收参数
  local host_addr="$1"

  # 传输文件
  containerd_scp_cmd_remote "${host_addr}"
  containerd_scp_plugins_remote "${host_addr}"
  containerd_scp_service_remote "${host_addr}"
  containerd_scp_config_remote "${host_addr}"
  buildkit_scp_files_remote "${host_addr}"
  print::msg "all" "success" "向节点 ${host_addr} 主机传输containerd的相关文件成功!!!"
}

# 部署containerd功能函数
containerd_services_start_remote(){
  # 接收参数
  local host_addr="$1"
  local service_name="${containerd_service}"

  # 启动服务
  remote_service_init "${host_addr}" "${service_name}"
}

# 部署buildkit功能函数
buildkit_services_start_remote(){
  # 接收参数
  local host_addr="$1"
  local service_name="buildkit.service"

  # 启动服务
  if [ "${containerd_build_func}" == "yes" ]; then
    remote_service_init "${host_addr}" "${service_name}"
  fi
}

# 定制nginx专属代理配置功能函数
nginx_proxy_conf_create_func(){
  # 接收参数
  local domain_name="$1"
  local proxy_conf="${nginx_ls_dir}/${domain_name}.conf"
  local nginx_cert_dir="/etc/nginx/certs"

  # 创建配置
  cat > "${proxy_conf}"<<-eof
server {
     listen 443 ssl;
     server_name ${domain_name};

     # 证书的定制
     ssl_certificate ${nginx_cert_dir}/${domain_name}.cert;
     ssl_certificate_key ${nginx_cert_dir}/${domain_name}.key;
     ssl_session_timeout 5m;
     ssl_session_cache   shared:SSL:10m;
     ssl_prefer_server_ciphers on;

     location / {
       proxy_pass http://127.0.0.1;
     }
 }
eof
}

# 定制传输nginx代理配置到远程主机功能函数
nginx_scp_conf_remote(){
  # 接收参数
  local host_addr="$1"
  local domain_name="$2"
  local s_conf_file="${nginx_ls_dir}/${domain_name}.conf"
  local d_conf_dir="/etc/nginx/conf.d"
 
  # 创建远程目录
  # remote_create_not_exist_dir_logic "${host_addr}" "${d_conf_dir}"
  file_oper_logic "${host_addr}" "create" "dir" "${d_conf_dir}"

  # 传输文件
  scp_file_base_func "${host_addr}" "${s_conf_file}" "${d_conf_dir}"
}

# 定制nginx的https配置功能函数
nginx_https_conf_func(){
  # 接收参数
  local host_addr="$1"
  local domain_name="$2"

  # 删除默认配置
  nginx_default_conf_delete_func "${host_addr}"

  # 创建专属代理配置
  nginx_proxy_conf_create_func "${domain_name}"

  # 传输配置到远程主机
  nginx_scp_conf_remote "${host_addr}" "${domain_name}"
}

# 传输nginx依赖的ca相关文件功能函数
nginx_depend_ca_scp_func(){
  # 接收参数
  local host_addr="$1"
  local domain_name="$2"
  local s_cert_file="${c_ls_ca_dir}/${domain_name}.cert"
  local s_key_file="${c_ls_ca_dir}/${domain_name}.key"
  local d_cert_dir="/etc/nginx/certs"

  # 创建远程目录
  # remote_create_not_exist_dir_logic "${host_addr}" "${d_cert_dir}"
  file_oper_logic "${host_addr}" "create" "dir" "${d_cert_dir}"

  # 传递ca文件到远程主机
  scp_file_base_func "${host_addr}" "${s_cert_file}" "${d_cert_dir}"
  scp_file_base_func "${host_addr}" "${s_key_file}" "${d_cert_dir}"
}

# 

# 部署nginx方式的harbor代理功能函数
harbor_https_use_nginx(){
  # 接收参数
  local host_addr="$1"
  local domain_name="$2"
  local service_name="nginx.service"

  if [ "${containerd_build_func}" == "yes" ]; then
    # 远程主机准备软件源文件
    nginx_repo_scp_remote "${host_addr}"
  
    # 部署nginx环境
    nginx_deploy_logic "${host_addr}" 
  
    # 定制nginx配置
    nginx_https_conf_func "${host_addr}" "${domain_name}"
  
    # 传递依赖的CA文件
    nginx_depend_ca_scp_func "${host_addr}" "${domain_name}"
    
    # 启动nginx服务
    remote_service_init "${host_addr}" "${service_name}"

    print::msg "all" "success" "k8s集群专属harbor镜像仓库https升级成功!!!"
  fi
  
} 

# 准备containerd部署相关文件功能函数
containerd_base_file_prepare(){
  # 准备containerd部署相关文件
  untar_containerd_files
  create_cntainerd_conf
  containerd_config_edit
}

# 准备buildkit部署相关文件功能函数
buildkit_base_file_prepare(){
  # 解压文件
  untar_nerdctl_file
  # 生成配置
  buildkit_config_edit
}

# 远程目标buildkit服务检测功能函数
remote_buildkit_service_check_logic(){
  # 接收参数
  local hosts_list="$*"

  # 定制变量
  b_deploy_list=""
  b_not_deploy_list=""

  # 检测远程主机服务状态
  if [[ ! "${hosts_list}" =~ ^[[:space:]]*$ ]]; then
    # 检测buildkit服务状态
    local service_name="${buildkit_service}"
    if [ "${containerd_build_func}" == "yes" ]; then
      for ip in ${hosts_list}; do
        local buildkit_status=$(remote_service_status_check "${ip}" "${service_name}")
        if [ "${buildkit_status}" == "active" ]; then
          print::msg "all" "warning" "节点 ${ip} 主机buildkit服务已部署成功，无需重复部署!!!"
          b_deploy_list="${b_deploy_list} ${ip}"
        else
          print::msg "all" "warning" "节点 ${ip} 主机buildkit服务未部署成功，接下来会进行部署!!!"
          b_not_deploy_list="${b_not_deploy_list} ${ip}"
        fi
      done
    fi
  fi
}

# 远程目标containerd服务检测功能函数
remote_containerd_service_check_logic(){
  # 接收参数
  local hosts_list="$*"

  # 定制变量
  c_deploy_list=""
  c_not_deploy_list=""

  # 检测远程主机状态
  local service_name="${containerd_service}"
  for ip in ${hosts_list}; do
    local containerd_status=$(remote_service_status_check "${ip}" "${service_name}")
    if [ "${containerd_status}" == "active" ]; then
      print::msg "all" "warning" "节点 ${ip} 主机containerd服务已部署成功，无需重复部署!!!"
      c_deploy_list="${c_deploy_list} ${ip}"
    else
      print::msg "all" "warning" "节点 ${ip} 主机containerd服务未部署成功，接下来会进行部署!!!"
      c_not_deploy_list="${c_not_deploy_list} ${ip}"
    fi
  done
}

# 部署libseccomp功能函数
remote_libseccomp_deploy_logic(){
  # 接收参数
  local host_addr="$1"

  # 判断远程主机操作系统类型
  local os_type=$(get_remote_os_type "remote" "${host_addr}")
  if [ "${os_type}" == "CentOS" ];then
    local s_file="${libsec_dir}/${libsec_tar}"
    local d_dir="/tmp"
    scp_file_base_func "${host_addr}" "${s_file}" "${d_dir}"
    local remote_cmd="yum install -y /tmp/${libsec_tar}"
    remote_node_exec_cmd "${host_addr}" "${remote_cmd}"
  fi
}

# 批量部署containerd服务功能函数
remote_containerd_deploy_logic(){
  # 接收参数
  local hosts_list="$*"

  # 基础配置的生成
  containerd_base_file_prepare

  # 批量部署containerd服务
  for ip in ${hosts_list}; do
    # 部署containerd
    containerd_scp_files_remote "${ip}"
    # Centos系统部署libseccomp
    remote_libseccomp_deploy_logic "${ip}"
    # 启动containerd
    containerd_services_start_remote "${ip}"
    # 部署buildkit
    buildkit_services_start_remote "${ip}"

    print::msg "all" "success" "节点 ${ip} 主机部署containerd服务成功!!!"
  done
}

# 批量部署buildkit服务功能函数
remote_buildkit_deploy_logic(){
  # 接收参数
  local hosts_list="$*"

  # 基础配置的生成
  # 所有containerd都部署好，仅有个别buildkit服务没部署的时候
  # 如果有containerd部署，那么没有必要重复解压nerdctl的软件等操作
  if [[ "${c_not_deploy_list}" =~ ^[[:space:]]*$ ]]; then
    buildkit_base_file_prepare
  fi

  # 批量部署buildkit服务
  for ip in ${hosts_list}; do
    # 部署buildkit
    buildkit_scp_files_remote "${ip}"
    # 启动buildkit
    buildkit_services_start_remote "${ip}"
    print::msg "all" "success" "节点 ${ip} 主机部署buildkit服务成功!!!" 
  done
}

# 检测nginx服务功能函数
remote_nginx_service_check_logic(){
  # 接收参数
  local host_addr="$1"

  # 进行nginx服务的检测
  if [ "${containerd_build_func}" == "yes" ]; then
    local service_name="${nginx_service}"
    local nginx_status=$(remote_service_status_check "${host_addr}" "${service_name}")
    if [ "${nginx_status}" == "active" ];then
      print::msg "all" "warning" "节点 ${host_addr} 主机nginx服务已部署成功，无需重复部署!!!"
    else
      print::msg "all" "warning" "节点 ${host_addr} 主机开始实施harbor仓库的https改造操作"
      harbor_https_use_nginx "${host_addr}" "${host_addr}"
      print::msg "all" "success" "节点 ${host_addr} 主机基于nginx实现https效果升级成功!!!"
    fi
  fi
}

# 远程目标服务检测功能函数
remote_containerd_service_status(){
  # 接收参数
  local hosts_list="$*"
  
  # 检测远程主机containerd的服务状态
  remote_containerd_service_check_logic "${hosts_list}"

  # 如果c_deploy_list 内容不为空,则检测buildkit服务是否部署成功
  remote_buildkit_service_check_logic "${c_deploy_list}"

  # 如果 c_not_deploy_list 内容为空，则表示所有集群主机containerd服务都部署成功
  if [[ "${c_not_deploy_list}" =~ ^[[:space:]]*$ ]]; then
    if [[ "${b_not_deploy_list}" =~ ^[[:space:]]*$ ]]; then
      print::msg "all" "warning" "k8s集群节点containerd容器环境已部署成功，无需重复部署!!!"
    fi
  else
    remote_containerd_deploy_logic "${c_not_deploy_list}"
  fi
   
  # 已部署containerd但是未部署buildkit的服务处理
  if [[ ! "${b_not_deploy_list}" =~ ^[[:space:]]*$ ]]; then
    remote_buildkit_deploy_logic "${b_not_deploy_list}"
  fi
}

# 主函数
main(){
  # 接收参数
  local hosts_list="$*"

  # 部署相关的环境
  remote_containerd_service_status "${hosts_list}"
  print::msg "all" "success" "k8s集群节点containerd容器环境部署成功!!!"
  
  # 进行harbor的nginx服务的检测
  remote_nginx_service_check_logic "${harbor_addr}"
}

main "${c_hostlist}"
