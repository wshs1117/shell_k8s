#!/bin/bash
# *************************************
# 功能: Shell 脚本信息输出
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-07-18
# *************************************

# 定制基础变量
log_file='/tmp/deploy.log'
log_info="正常"
log_succ="成功"
log_warn="提示"
log_erro="异常"

#  echo -e "\e[33m开始执行离线方式部署K8s集群的基础操作...\e[0m"

# 定制正常信息
log::info(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;37m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_info} >> ${log_file}
}

# 定制成功信息
log::success(){
  # 接收参数
  local log_msg="$@"
  printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;32m[%-10s]\e[0m: ${log_msg}\n" \
         '  '${log_succ} >> ${log_file}
}

# 仅终端输出
console::info(){
  # 接收参数
  local log_msg="$@"
  printf "\e[1;37m[%-10s]\e[0m: ${log_msg}\n" '  '${log_info}
}

console::success(){
  # 接收参数
  local log_msg="$@"
  printf "\e[1;32m[%-10s]\e[0m: ${log_msg}\n" '  '${log_succ}
}

# 场景输出信息
print::info(){
   # 接收参数
  local log_msg="$@"

  console::info "${log_msg}"
  log::info "${log_msg}"
}

print::success(){
   # 接收参数
  local log_msg="$@"

  console::success "${log_msg}"
  log::success "${log_msg}"
}

log::info "1 dsfadsaf fdsaf fdasfds"
log::success "2 dsfadsaf fdsaf fdasfds"
echo "=========="
console::info "3 dsfadsaf fdsaf fdasfds"
console::success "4 dsfadsaf fdsaf fdasfds"
echo "++++++++++"
print::info "5 dsfadsaf fdsaf fdasfds"
print::success "6 dsfadsaf fdsaf fdasfds"
