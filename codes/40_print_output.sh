#!/bin/bash
# *************************************
# 功能: Shell 脚本信息输出
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-07-18
# *************************************

# 定制基础变量
log_file='/tmp/deploy.log'
log_info="正常"
log_succ="成功"
log_warn="提示"
log_erro="异常"
# log_format='\e[1;37m[%-10s]\e[0m:\n'

#  echo -e "\e[33m开始执行离线方式部署K8s集群的基础操作...\e[0m"

get_color_num(){
  # 接收参数
  local msg_level="$1"

  if [ "${msg_level}" == "异常" ]; then
    local msg_color_num="31"
  elif [ "${msg_level}" == "提示"  ]; then
    local msg_color_num="33"
  elif [ "${msg_level}" == "成功"  ]; then
    local msg_color_num="32"
  elif [ "${msg_level}" == "正常"  ]; then
    local msg_color_num="37"
  fi
  echo "${msg_color_num}"
}

print::msg(){
  # 接收参数
  local msg_type="$1"
  local msg_level="$2"
  local log_msg=$(echo "$@" | awk '{$1=null; $2=null; print $0}'| awk '$1=$1')
  local msg_color_num=$(get_color_num "${msg_level}")
  # printf "$(date "+%Y-%m-%d %T %Z(%z)") \e[1;37m[%-10s]\e[0m: ${log_msg}\n" '  '${log_info}
  local msg_format="\e[1;${msg_color_num}m[  ${msg_level}  ]\e[0m:"
  if [ "${msg_type}" == "log" ];then
    printf "$(date "+%Y-%m-%d %T %Z(%z)") ${msg_format} ${log_msg}\n" >> ${log_file}
  elif [ "${msg_type}" == "console" ];then
    printf "${msg_format} ${log_msg}\n"
  fi
}

# 场景输出函数
print::msg_bak(){
  # 接收参数
  local msg_level="$1"
  local msg_context=$(echo "$@" | awk '{$1=null; print $0}')

  # 打印信息
  console::${msg_level} "${msg_context}"
  log::${msg_level} "${msg_context}"
}

print::msg log "正常" "1 dsafds dfad dfafdsaf"
print::msg console "正常" "2 dsafds dfad dfafdsaf"
print::msg log "成功" "3 dsafds dfad dfafdsaf"
print::msg console "成功" "4 dsafds dfad dfafdsaf"
print::msg log "异常" "5 dsafds dfad dfafdsaf"
print::msg console "异常" "6 dsafds dfad dfafdsaf"
print::msg log "提示" "7 dsafds dfad dfafdsaf"
print::msg console "提示" "8 dsafds dfad dfafdsaf"
