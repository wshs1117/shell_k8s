#!/bin/bash
# *************************************
# 功能: Shell加速配置
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-09-15
# *************************************

# 定制ssh加速链接的配置
ssh_acceleration_link_logic(){
  # 定制变量
  local ssh_conf="$HOME/.ssh/config"

  # 定制加速配置
  cat > "${ssh_conf}"<<-eof
Host *
    Protocol 2
    ServerAliveInterval 60
    ServerAliveCountMax 30
    ControlMaster auto
    ControlPersist 10m
    ControlPath ~/.ssh/connect-%r@%h:%p.socket
eof
}

ssh_acceleration_link_logic
