#!/bin/bash
# *************************************
# 功能: Kubernetes集群清理管理
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2024-01-28
# *************************************

# 加载项目的配置属性信息
source_proj_base_conf(){
  # 获取当前项目的根路径
  root_dir=$(dirname $PWD)

  # 确保项目存在基础配置文件
  if [ ! -f $root_dir/conf/config ]; then
    cp $root_dir/tmpl/* $root_dir/conf/
  fi

  # 加载基本环境属性
  source $root_dir/conf/config
}

# 加载子函数
source_proj_base_conf
source ${subfunc_dir}/${base_func_exec}
source ${subfunc_dir}/${base_func_menu}
source ${subfunc_dir}/${base_func_usage}
source ${subfunc_dir}/${k8s_func_exec}
source ${subfunc_dir}/${base_func_image}
source ${subfunc_dir}/${base_cluster_exec}
source ${subfunc_dir}/${k8s_subfunc_manager}
source ${subfunc_dir}/${k8s_subfunc_clean}
source ${subfunc_dir}/${k8s_subfunc_network}

# 基础内容
array_target=(one清 one重 net清理 n清理 m清理 c清理 k清理 r清理 h清理 s清理 退出)

# 主函数
main(){
  while true
  do
    # 打印k8s集群清理管理菜单
    k8s_cluster_clean_manager
    read -p "请输入您要操作的选项id值: " target_id
    [ -e ${target_id} ] && target_id='100'
    local num_type=$(user_input_datatype_confirm "${target_id}")
    if [ "${num_type}" != "is_num" ]; then
      echo -e "\e[31m请输入正确格式的内容信息...\e[0m"
      print::msg "console" "error" "请输入正确格式的内容信息..."
      continue
    fi
    if [ ${#array_target[@]} -ge ${target_id} ]; then
      if [ ${array_target[$target_id-1]} == "one清" ]; then
         print::msg "all" "warning" "开始执行k8s集群一键清理操作..."
         harbor_clean_status=$(user_define_xxx_type "是否同时清理harbor" "yes" "no" "yes")
         k8s_cluster_one_key_clean "${harbor_clean_status}"
      elif [ ${array_target[$target_id-1]} == "one重" ]; then
         print::msg "all" "warning" "开始执行k8s集群一键重置操作..."
         k8s_cluster_one_key_reset
      elif [ ${array_target[$target_id-1]} == "net清理" ]; then
         print::msg "all" "warning" "开始执行k8s集群网络环境清理操作..."
         local current_network_type=$(k8s_cluster_get_network_type)
         print::msg "console" "warning" "k8s集群当前的网络解决方案是: ${current_network_type}"
         del_net=$(user_define_xxx_type "是否要清理当前的网络方案" "yes" "no" "yes")
         if [ "${del_net}" == "yes" ]; then
           k8s_cluster_network_clean "${current_network_type}"
         elif [ "${del_net}" == "no" ]; then
           print::msg "all" "warning" "暂不删除当前k8s集群的网络解决方案..."
         else
           print::msg "console" "error" "请不要输入无效的信息..."
         fi
      elif [ ${array_target[$target_id-1]} == "n清理" ]; then
         print::msg "all" "warning" "开始执行k8s集群工作节点清理操作..."
         k8s_cluster_worker_node_clean
      elif [ ${array_target[$target_id-1]} == "m清理" ]; then
         print::msg "all" "warning" "开始执行k8s集群控制节点清理操作..."
         k8s_cluster_master_node_remove
      elif [ ${array_target[$target_id-1]} == "c清理" ]; then
         print::msg "all" "warning" "开始执行k8s集群容器环境清理操作..."
         k8s_cluster_node_container_clean
      elif [ ${array_target[$target_id-1]} == "k清理" ]; then
         print::msg "all" "warning" "开始执行k8s集群内核环境清理操作..."
         k8s_cluster_node_sysconf_clean
      elif [ ${array_target[$target_id-1]} == "r清理" ]; then
         print::msg "all" "warning" "开始执行k8s集群镜像仓库清理操作..."
         k8s_cluster_node_harbor_clean
      elif [ ${array_target[$target_id-1]} == "h清理" ]; then
         print::msg "all" "warning" "开始执行k8s集群高可用环境清理操作..."
      elif [ ${array_target[$target_id-1]} == "s清理" ]; then
         print::msg "all" "warning" "开始执行k8s集群ssh环境清理操作..."
         k8s_cluster_node_ssh_clean
      elif [ ${array_target[$target_id-1]} == "退出" ]; then
         print::msg "all" "warning" "开始执行K8s集群清理管理退出操作..."
         exit
      fi
    else
      Usage
    fi
  done
}

# 执行主函数
main
