#!/bin/bash
# *************************************
# 功能: 提取相关属性信息，而后修改内容生成文件
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 微信群: 自学自讲—软件工程
# 抖音号: sswang_yys 
# 版本: v0.1
# 日期: 2024-07-24
# *************************************


# 定制环境变量
local_config_file="tmpl/config"
local_hosts_file="tmpl/hosts"

target_config_file="conf/config"
target_hosts_file="conf/hosts"

# 提取相关信息
get_config_attr_value(){
  # 接收参数
  local key="$1"
  local file="$2"

  grep "^${key}=" "${file}" | awk -F'"' '{print $2}'
}

# 格式化输出信息
print_msg(){
  # 接收参数
  local num="$1"
  local key="$2"
  local value="$3"
  
  # 格式化输出
  printf "|   %-2d   |  %-15s|  %-12s| %-25s |\n" "${num}" "${key}" "${value}" "${array_remark[$num-1]}"
}

print_header(){
  echo "                   k8s集群部署基础属性信息列表"
  echo "-------------------------------------------------------------------"
  printf "| %7s |  %-15s| %-13s |   %-23s   |\n" "序列号" "功能条目" "  功能属性  " "       备注"
  echo "-------------------------------------------------------------------"
}

print_tail()(
  echo "-------------------------------------------------------------------"
  echo "注意：目前脚本项目功能,支持下列选项: "
  echo "      集群模式：仅支持单主集群    容器引擎：仅支持docker类型"
  echo "      仓库协议：仅支持http类型    网络方案：仅支持flannel|calico类型"
  echo "      网络模式：仅支持iptables类型 "
)

print_body(){
  # 接收参数
  local num="$1"
  local context="$2"
  local value="$3"

  print_msg "${num}" "${context}" "${value}"
}


# 
print_hosts_header(){
  echo "                k8s集群部署主机列表信息列表"
  echo "------------------------------------------------------------"
  printf "| %7s |  %-15s| %-13s |  %-10s  |\n" "序列号" "主机地址" "      短主机名      " "主机角色"
  echo "------------------------------------------------------------"
}

# 获取主机角色信息
get_hosts_role(){
  # 接收参数
  local hostname="$1"
  
  # 判断逻辑
  if [[ "${hostname}" =~ .*node.* ]]; then
    local hostrole="node"
  elif [[ "${hostname}" =~ .*master.* ]]; then
    local hostrole="master"
  elif [[ "${hostname}" =~ .*register.* ]]; then
    local hostrole="register"
  else
    local hostrole="other"
  fi
  
  echo "${hostrole}"
}

# 格式化输出信息
print_hosts_msg(){
  # 接收参数
  local num="$1"
  local ip="$2"
  local hostname="$3"
  local role=$(get_hosts_role "${hostname}")
  # 格式化输出
  printf "|   %-2d   |  %-11s|  %-20s| %-10s |\n" "${num}" "${ip}" "${hostname}" "${role}"
}

# 打印主机列表清单
print_hosts_body(){
  # 接收参数
  local num="$1"
  local ip="$2"
  local hostname="$3"

  print_hosts_msg "${num}" "${ip}" "${hostname}"
}

print_hosts_tail()(
  echo "------------------------------------------------------------"
  echo "提示: 目前脚本项目暂不支持自动修改主机列表其他内容"
  echo "      如需修改，请直接编辑hosts文件"
)


# 定制数组
array_context=(集群版本 集群模式 部署方式 容器引擎 网段信息 仓库协议 网络方案 网络模式)
array_key=(k8s_version cluster_type default_deploy_type default_container_engine_type target_net harbor_http_type default_network_type default_network_proxy)
array_remark=(
             "1.6+"
             "alone|multi"
             "online|offline"
             "docker|containerd|cri-o"
             "10.0.0"
             "http|https"
             "flannel|calico|cilium"
             "iptables|ipvs"
             )

# 定制新的修改内容的临时保存数组
declare -a array_key
declare -A array_edit
array_kv=""
for i in $(seq ${#array_key[@]}); do
  value=$(get_config_attr_value "${array_key[$i-1]}" "${local_config_file}")
  array_kv="${array_kv} [${array_key[$i-1]}]=${value}"
done
array_define="declare -A array_edit=(${array_kv})"
eval "${array_define}"

# 打印信息
auto_print_conf_msg(){
  # 格式化输出信息
  print_header
  for i in $(seq ${#array_context[@]});do
    value=$(get_config_attr_value "${array_key[$i-1]}" "${local_config_file}")
    print_body "${i}" "${array_context[$i-1]}" "${value}"
  done
  print_tail
}
  
# auto_print_msg
auto_print_hosts_msg(){
  # 获取网段信息
  target_net=$(get_config_attr_value "target_net" "${target_config_file}")
  let num=1
  # 格式化输出信息
  print_hosts_header
  for ip in $(grep "${target_net}" "${target_hosts_file}" | awk '{print $1}');do
    local hostname=$(grep $ip "${target_hosts_file}" | awk '{print $2}')
    print_hosts_body "${num}" "${ip}" "${hostname}"
    let num+=1
  done
  print_hosts_tail
}
# auto_print_hosts_msg

# 自动打印修改后信息
auto_print_conf_edit_msg(){
  # 格式化输出信息
  print_header
  for i in $(seq ${#array_context[@]});do
    print_body "${i}" "${array_context[$i-1]}" "${array_edit[${array_key[$i-1]}]}"
  done
  print_tail
}

# 判断用户输入信息是否是数字或字符串
user_input_datatype_confirm(){
  # 接收参数
  local input_context="$1"

  # 数据格式判断
  if [[ "${input_context}" =~ ^[0-9]+$ ]]; then
    local num_type="is_num"
  elif [[ "${input_context}" =~ ^[0-9a-Z]$ ]]; then
    local num_type="is_string"
  else
    local num_type="other"
  fi

  echo "${num_type}"
}

# 修改具体属性值
edit_config_one_attr(){
  # 接收参数
  
  local context="${array_context[$1-1]}"
  local key="${array_key[$1-1]}"
  
  read -p "请输入您要修改的 ${context} 属性值: " value_new
  array_edit[${key}]=${value_new}
}

# 保存属性配置信息
edit_after_save_file(){
  # 1 生成配置文件
  cp "${local_config_file}" "${target_config_file}" 
  cp "${local_hosts_file}" "${target_hosts_file}" 
  
  # 2 修改属性信息
  for key in ${!array_edit[@]}; do
    value="${array_edit[${key}]}"
    sed -i "/^${key}=/s@${key}=.*@${key}=\"${value}\"@" "${target_config_file}"
  done
  
  # 3 修改hosts文件的网段信息
  old_target_net=$(get_config_attr_value "target_net" "${local_config_file}")
  target_net=$(get_config_attr_value "target_net" "${target_config_file}")
  if [ "${old_target_net}" != "${target_net}" ]; then
    sed -i "s@${old_target_net}@${target_net}@g" "${target_hosts_file}"
  fi
}

# 编辑相关的属性信息
edit_config_attr(){
  # 
  auto_print_conf_msg
  while true;do
    read -p "请输入您要修改的属性ID(0表示退出): " attr_id
    input_status=$(user_input_datatype_confirm "${attr_id}")
    if [ "${input_status}" != "is_num" ]; then
      echo "请输入有效的ID"
    elif [ "${input_status}" == "is_num" ]; then
      if [ "${attr_id}" == "0" ]; then
        edit_after_save_file
        echo "退出"
        break
      elif [ "${attr_id}" -gt "${#array_context[@]}" ];then
        echo "请输入有效的ID"
      else
        edit_config_one_attr "${attr_id}"
        auto_print_conf_edit_msg
      fi
    fi
  done
}
edit_config_attr
auto_print_hosts_msg

# edit_hosts_attr(){
  
# }
# edit_config_attr
