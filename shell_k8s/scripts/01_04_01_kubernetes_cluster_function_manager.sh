#!/bin/bash
# *************************************
# 功能: Kubernetes集群功能管理
# 作者: 王树森
# 联系: wangshusen@sswang.com
# 版本: v0.1
# 日期: 2024-01-28
# *************************************

# 加载项目的配置属性信息
source_proj_base_conf(){
  # 获取当前项目的根路径
  root_dir=$(dirname $PWD)

  # 确保项目存在基础配置文件
  if [ ! -f $root_dir/conf/config ]; then
    cp $root_dir/tmpl/* $root_dir/conf/
  fi

  # 加载基本环境属性
  source $root_dir/conf/config
}

# 加载子函数
source_proj_base_conf
source ${subfunc_dir}/${base_func_exec}
source ${subfunc_dir}/${base_func_menu}
source ${subfunc_dir}/${base_func_usage}
# 引入三个函数库，目的是定制多文件中的一键功能
source ${subfunc_dir}/${base_cluster_exec}
source ${subfunc_dir}/${k8s_func_exec}
source ${subfunc_dir}/${base_func_image}
source ${subfunc_dir}/${k8s_subfunc_manager}
source ${subfunc_dir}/${k8s_subfunc_clean}

# 基础内容
array_target=(n扩缩 m扩缩 n升级 m升级 证书 备份 还原 一键 退出)

# 主函数
main(){
  while true
  do
    # 打印k8s集群功能管理菜单
    k8s_cluster_function_manager
    read -p "请输入您要操作的选项id值: " target_id
    [ -e ${target_id} ] && target_id='100'
    local num_type=$(user_input_datatype_confirm "${target_id}")
    if [ "${num_type}" != "is_num" ]; then
      print::msg "console" "error" "请输入正确格式的内容信息..."
      continue
    fi
    if [ ${#array_target[@]} -ge ${target_id} ]; then
      if [ ${array_target[$target_id-1]} == "n扩缩" ]; then
        print::msg "all" "warning" "开始执行k8s集群工作节点扩缩容操作..."
        read -p "请输入您要操作节点的动作类型(扩容-out|缩容-in): " opt_type
        [ "${opt_type}" == "in" ] && scale_in_node "reset"
        [ "${opt_type}" == "out" ] && scale_out_node
      elif [ ${array_target[$target_id-1]} == "m扩缩" ]; then
        print::msg "all" "warning" "开始执行k8s集群控制节点扩缩容操作..."
      elif [ ${array_target[$target_id-1]} == "n升级" ]; then
        print::msg "all" "warning" "开始执行k8s集群工作节点升级操作..."
        k8s_cluster_nodes_update
      elif [ ${array_target[$target_id-1]} == "m升级" ]; then
        print::msg "all" "warning" "开始执行k8s集群控制节点升级操作..."
        # 获取k8s的版本信息
        k8s_version_list
        # 确认要更新的k8s版本
        read -t 10 -p "请输入要部署的k8s版本(比如：1.28.1，空表示使用默认值): " version
        [ -z $version ] && update_ver="v${k8s_version}" || update_ver="v${version}"
        # 确认是否更新k8s的时候，同步更新ETCD
        etcd_update_status=$(user_define_xxx_type "是否同步更新ETCD环境" "true" "false" "true")
        # 根据当前集群类型，确定要更新的节点地址
        if [ "${cluster_type}" == "multi" ]; then
          ip_list=$(user_define_create_ip_list "更新控制节点")
        else
          ip_list=${master1}
        fi
        # 执行k8s集群控制节点更新
        k8s_master_update "${version}" "${update_ver}" "${etcd_update_status}" "${ip_list}"
      elif [ ${array_target[$target_id-1]} == "证书" ]; then
        print::msg "all" "warning" "开始执行k8s集群证书升级操作..."
        cert_update_type=$(user_define_xxx_type "更新k8s集群证书" "renew" "openssl" "none" "renew")
        k8s_cert_update "${cert_update_type}"
      elif [ ${array_target[$target_id-1]} == "备份" ]; then
        print::msg "all" "warning" "开始执行k8s集群数据备份操作..."
        k8s_data_save
      elif [ ${array_target[$target_id-1]} == "还原" ]; then
        print::msg "all" "warning" "开始执行k8s集群数据还原操作..."
        k8s_data_restore
      elif [ ${array_target[$target_id-1]} == "一键" ]; then
        print::msg "all" "warning" "开始执行k8s集群一键升级操作..."
        # 获取k8s的版本信息
        k8s_version_list
        # 确认要更新的k8s版本
        read -t 10 -p "请输入要部署的k8s版本(比如：1.28.1，空表示使用默认值): " version
        [ -z $version ] && update_ver="v${k8s_version}" || update_ver="v${version}"
        # 确认是否更新k8s的时候，同步更新ETCD
        etcd_update_status=$(user_define_xxx_type "是否同步更新ETCD环境" "true" "false" "true")
        k8s_onekey_update "${version}" "${update_ver}" "${etcd_update_status}"
      elif [ ${array_target[$target_id-1]} == "退出" ]; then
        print::msg "all" "warning" "开始执行K8s集群功能管理退出操作..."
        exit
      fi
    else
      Usage
    fi
  done
}

# 执行主函数
main
